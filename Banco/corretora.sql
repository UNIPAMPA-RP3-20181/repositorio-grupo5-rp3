-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 18-Jul-2018 às 02:58
-- Versão do servidor: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `corretora`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `apolice`
--

CREATE TABLE `apolice` (
  `idApolice` int(11) NOT NULL,
  `premioApolice` double NOT NULL,
  `valorSinistroMorteApolice` double NOT NULL,
  `valorSinistroIncaApolice` double NOT NULL,
  `qntParcelaApolice` int(11) NOT NULL,
  `valorParcelaApolice` double NOT NULL,
  `dataPagamentoApolice` int(11) NOT NULL,
  `numeroCartaoApolice` int(11) NOT NULL,
  `nomeDonoCartaoApolice` varchar(45) NOT NULL,
  `bandeiraApolice` varchar(40) NOT NULL,
  `bancoApolice` varchar(45) NOT NULL,
  `cpfDonoApolice` varchar(20) NOT NULL,
  `quantidadeParcelaApolice` int(11) NOT NULL,
  `SolicitacaoSeguro_idSolicitacaoSeguro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliador`
--

CREATE TABLE `avaliador` (
  `idAvaliador` int(11) NOT NULL,
  `acesso` int(1) NOT NULL,
  `DataContratacaoAvaliador` date DEFAULT NULL,
  `AtivoAvaliador` tinyint(4) NOT NULL,
  `nomeAvaliador` varchar(30) NOT NULL,
  `ativoAvaliador2` tinyint(4) DEFAULT NULL,
  `telefone` varchar(20) NOT NULL,
  `Usuario_idUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `beneficiario`
--

CREATE TABLE `beneficiario` (
  `idBeneficiario` int(11) NOT NULL,
  `nomeBeneficiario` varchar(150) DEFAULT NULL,
  `emailBeneficiario` varchar(45) DEFAULT NULL,
  `parentescoBeneficiario` varchar(20) DEFAULT NULL,
  `contatoBeneficiario` varchar(50) DEFAULT NULL,
  `Candidato_idCandidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `beneficiario`
--

INSERT INTO `beneficiario` (`idBeneficiario`, `nomeBeneficiario`, `emailBeneficiario`, `parentescoBeneficiario`, `contatoBeneficiario`, `Candidato_idCandidato`) VALUES
(8, 'Juca da Silva Fulano', 'juquinha@gmail.com', 'Filho(a)', '(55)55555-5555', 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `candidato`
--

CREATE TABLE `candidato` (
  `idCandidato` int(11) NOT NULL,
  `acesso` int(1) NOT NULL,
  `ocupacaoCandidato` varchar(100) DEFAULT NULL,
  `Avaliador_idAvaliador` int(11) DEFAULT NULL,
  `Endereco_idEndereco` int(11) NOT NULL,
  `Usuario_idUsuario` int(11) NOT NULL,
  `Pessoa_idPessoa` int(11) NOT NULL,
  `imagePerfil` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `candidato`
--

INSERT INTO `candidato` (`idCandidato`, `acesso`, `ocupacaoCandidato`, `Avaliador_idAvaliador`, `Endereco_idEndereco`, `Usuario_idUsuario`, `Pessoa_idPessoa`, `imagePerfil`) VALUES
(7, 1, 'Mecanico', NULL, 29, 19, 4, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `corretor`
--

CREATE TABLE `corretor` (
  `idCorretor` int(11) NOT NULL,
  `acesso` int(1) NOT NULL,
  `nomeCorretor` varchar(100) NOT NULL,
  `DataContratacaoCorretor` date DEFAULT NULL,
  `AtivoCorretor` tinyint(4) NOT NULL,
  `Usuario_idUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE `endereco` (
  `idEndereco` int(11) NOT NULL,
  `RuaEndereco` varchar(250) DEFAULT NULL,
  `NumeroEndereco` varchar(15) DEFAULT NULL,
  `CepEndereco` varchar(30) DEFAULT NULL,
  `BairroEndereco` varchar(100) DEFAULT NULL,
  `CidadeEndereco` varchar(100) DEFAULT NULL,
  `EstadoEndereco` varchar(100) DEFAULT NULL,
  `Complemento` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `endereco`
--

INSERT INTO `endereco` (`idEndereco`, `RuaEndereco`, `NumeroEndereco`, `CepEndereco`, `BairroEndereco`, `CidadeEndereco`, `EstadoEndereco`, `Complemento`) VALUES
(29, 'Rua General Ciclano', '124', '55555-555', 'Leblon', 'Uberaba', 'MG', 'apto 202');

-- --------------------------------------------------------

--
-- Estrutura da tabela `itemproblemasaude`
--

CREATE TABLE `itemproblemasaude` (
  `idItemProblemaSaude` int(11) NOT NULL,
  `ResultadoIPS` varchar(50) DEFAULT NULL,
  `ObservacaoIPS` varchar(300) DEFAULT NULL,
  `ProblemaSaude_idProblemaSaude` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `itemproblemasaude`
--

INSERT INTO `itemproblemasaude` (`idItemProblemaSaude`, `ResultadoIPS`, `ObservacaoIPS`, `ProblemaSaude_idProblemaSaude`) VALUES
(22, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `idPessoa` int(11) NOT NULL,
  `nomePessoa` varchar(250) DEFAULT NULL,
  `cpfPessoa` varchar(20) DEFAULT NULL,
  `telefonePessoa` varchar(20) DEFAULT NULL,
  `celularPessoa` varchar(20) DEFAULT NULL,
  `sexoPessoa` varchar(30) DEFAULT NULL,
  `estadoCivilPessoa` varchar(50) DEFAULT NULL,
  `dataPessoa` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`idPessoa`, `nomePessoa`, `cpfPessoa`, `telefonePessoa`, `celularPessoa`, `sexoPessoa`, `estadoCivilPessoa`, `dataPessoa`) VALUES
(4, 'João Candidato', '101.043.026-23', '(55)55555-5555', '(55)55555-5555', 'Masculino', 'Casado(a)', '13/12/1982');

-- --------------------------------------------------------

--
-- Estrutura da tabela `problemasaude`
--

CREATE TABLE `problemasaude` (
  `idProblemaSaude` int(11) NOT NULL,
  `nomeProblemaSaude` varchar(50) DEFAULT NULL,
  `descricaoProblemaSaude` varchar(300) DEFAULT NULL,
  `idItemProblemaSaude` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `problemasaude`
--

INSERT INTO `problemasaude` (`idProblemaSaude`, `nomeProblemaSaude`, `descricaoProblemaSaude`, `idItemProblemaSaude`) VALUES
(28, 'Enxaqueca', 'Dor de cabeça', 0),
(29, 'Joelhor bixado', 'Joelho ruim', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `relatomorte`
--

CREATE TABLE `relatomorte` (
  `nomeSegurado` varchar(220) NOT NULL,
  `numeroApolice` int(11) NOT NULL,
  `causaMorte` varchar(220) NOT NULL,
  `nomeContato` varchar(220) NOT NULL,
  `parentesco` varchar(220) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `email` varchar(220) NOT NULL,
  `idRelatoMorte` int(11) NOT NULL,
  `pdf` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `relatomorte`
--

INSERT INTO `relatomorte` (`nomeSegurado`, `numeroApolice`, `causaMorte`, `nomeContato`, `parentesco`, `telefone`, `email`, `idRelatoMorte`, `pdf`) VALUES
('Jose', 1001, 'morreu', 'Nao morto', 'parente', '(55)55555-5555', 'teste@teste.com', 1, 0x433a55736572730d6f6c6976446f776e6c6f61647331303333372d33393133382d312d50422e706466);

-- --------------------------------------------------------

--
-- Estrutura da tabela `segurado`
--

CREATE TABLE `segurado` (
  `idSegurado` int(11) NOT NULL,
  `CondicaoSegurado` tinyint(4) DEFAULT NULL,
  `IncapacitadoSegurado` tinyint(4) DEFAULT NULL,
  `Apolice_idApolice` int(11) NOT NULL,
  `Candidato_idCandidato` int(11) NOT NULL,
  `Candidato_Pessoa_idPessoa` int(11) NOT NULL,
  `Candidato_Pessoa_Endereco_idEndereco` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sinistro`
--

CREATE TABLE `sinistro` (
  `idSinistro` int(11) NOT NULL,
  `DescricaoSinistro` varchar(45) DEFAULT NULL,
  `ValorSinistro` double DEFAULT NULL,
  `autorizadoSinistro` tinyint(4) DEFAULT NULL,
  `nomeContatoSinistro` varchar(20) DEFAULT NULL,
  `telefoneSinistro` varchar(20) DEFAULT NULL,
  `parecerAvaliadorSinistro` varchar(100) DEFAULT NULL,
  `contatoSinistro` varchar(45) DEFAULT NULL,
  `cpfContatoSinistro` varchar(20) DEFAULT NULL,
  `Apolice_idApolice` int(11) NOT NULL,
  `TipoSinistro_idTipoSinistro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacaoseguro`
--

CREATE TABLE `solicitacaoseguro` (
  `idSolicitacaoSeguro` int(11) NOT NULL,
  `valorSolicitacaoSeguro` double DEFAULT NULL,
  `StatusSolicitacaoCorretor` int(1) DEFAULT NULL,
  `MotivoReprovacaoCorretor` varchar(200) DEFAULT NULL,
  `StatusSolicitacaoAvaliador` int(1) DEFAULT NULL,
  `MotivoReprovacaoAvaliador` varchar(200) DEFAULT NULL,
  `ValorCorrigidoSeguro` double DEFAULT NULL,
  `PredispCancerSeguro` int(11) DEFAULT NULL,
  `PredispDiabeteSeguro` int(11) DEFAULT NULL,
  `PredispDemenciaSeguro` int(11) DEFAULT NULL,
  `PredispCoracaoSeguro` int(11) DEFAULT NULL,
  `PredispCerebralSeguro` int(11) DEFAULT NULL,
  `PredispPulmonarSeguro` int(11) DEFAULT NULL,
  `PredispDegeneracaoSeguro` int(11) DEFAULT NULL,
  `alcoolatra` tinyint(1) DEFAULT NULL,
  `fumante` tinyint(4) DEFAULT NULL,
  `DataSolicitacao` varchar(20) DEFAULT NULL,
  `Candidato_idCandidato` int(11) NOT NULL,
  `Corretor_idCorretor` int(11) DEFAULT NULL,
  `ItemProblemaSaude_idItemProblemaSaude` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `solicitacaoseguro`
--

INSERT INTO `solicitacaoseguro` (`idSolicitacaoSeguro`, `valorSolicitacaoSeguro`, `StatusSolicitacaoCorretor`, `MotivoReprovacaoCorretor`, `StatusSolicitacaoAvaliador`, `MotivoReprovacaoAvaliador`, `ValorCorrigidoSeguro`, `PredispCancerSeguro`, `PredispDiabeteSeguro`, `PredispDemenciaSeguro`, `PredispCoracaoSeguro`, `PredispCerebralSeguro`, `PredispPulmonarSeguro`, `PredispDegeneracaoSeguro`, `alcoolatra`, `fumante`, `DataSolicitacao`, `Candidato_idCandidato`, `Corretor_idCorretor`, `ItemProblemaSaude_idItemProblemaSaude`) VALUES
(6, 883.81, 3, NULL, 3, NULL, NULL, 3, 0, 0, 0, 2, 5, 0, 0, 1, '17/07/2018', 7, NULL, 22);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tiposinistro`
--

CREATE TABLE `tiposinistro` (
  `idTipoSinistro` int(11) NOT NULL,
  `tipoSinistro` varchar(20) NOT NULL,
  `fatalidadeSinistro` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `nomeUsuario` varchar(45) NOT NULL,
  `senhaUsuario` varchar(45) NOT NULL,
  `emailUsuario` varchar(100) DEFAULT NULL,
  `tipoUsuario` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `nomeUsuario`, `senhaUsuario`, `emailUsuario`, `tipoUsuario`) VALUES
(19, 'candidato', 'candidato', 'teste123@teste.com', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apolice`
--
ALTER TABLE `apolice`
  ADD PRIMARY KEY (`idApolice`),
  ADD KEY `fk_Apolice_SolicitacaoSeguro1_idx` (`SolicitacaoSeguro_idSolicitacaoSeguro`);

--
-- Indexes for table `avaliador`
--
ALTER TABLE `avaliador`
  ADD PRIMARY KEY (`idAvaliador`),
  ADD KEY `fk_Avaliador_Usuario1_idx` (`Usuario_idUsuario`);

--
-- Indexes for table `beneficiario`
--
ALTER TABLE `beneficiario`
  ADD PRIMARY KEY (`idBeneficiario`),
  ADD KEY `fk_Beneficiario_Pessoa1` (`Candidato_idCandidato`);

--
-- Indexes for table `candidato`
--
ALTER TABLE `candidato`
  ADD PRIMARY KEY (`idCandidato`),
  ADD KEY `fk_Candidato_Avaliador1_idx` (`Avaliador_idAvaliador`),
  ADD KEY `fk_Candidato_Endereco1_idx` (`Endereco_idEndereco`),
  ADD KEY `fk_Candidato_Usuario1_idx` (`Usuario_idUsuario`),
  ADD KEY `fk_Candidato_Pessoa1` (`Pessoa_idPessoa`);

--
-- Indexes for table `corretor`
--
ALTER TABLE `corretor`
  ADD PRIMARY KEY (`idCorretor`),
  ADD KEY `fk_Corretor_Usuario1_idx` (`Usuario_idUsuario`);

--
-- Indexes for table `endereco`
--
ALTER TABLE `endereco`
  ADD PRIMARY KEY (`idEndereco`);

--
-- Indexes for table `itemproblemasaude`
--
ALTER TABLE `itemproblemasaude`
  ADD PRIMARY KEY (`idItemProblemaSaude`),
  ADD KEY `fk_ItemProblemaSaude_ProblemaSaude1_idx` (`ProblemaSaude_idProblemaSaude`);

--
-- Indexes for table `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`idPessoa`),
  ADD UNIQUE KEY `cpfPessoa` (`cpfPessoa`);

--
-- Indexes for table `problemasaude`
--
ALTER TABLE `problemasaude`
  ADD PRIMARY KEY (`idProblemaSaude`);

--
-- Indexes for table `relatomorte`
--
ALTER TABLE `relatomorte`
  ADD PRIMARY KEY (`idRelatoMorte`);

--
-- Indexes for table `segurado`
--
ALTER TABLE `segurado`
  ADD PRIMARY KEY (`idSegurado`,`Apolice_idApolice`,`Candidato_idCandidato`,`Candidato_Pessoa_idPessoa`,`Candidato_Pessoa_Endereco_idEndereco`),
  ADD KEY `fk_Segurado_Apolice1_idx` (`Apolice_idApolice`),
  ADD KEY `fk_Segurado_Candidato1_idx` (`Candidato_idCandidato`,`Candidato_Pessoa_idPessoa`,`Candidato_Pessoa_Endereco_idEndereco`);

--
-- Indexes for table `sinistro`
--
ALTER TABLE `sinistro`
  ADD PRIMARY KEY (`idSinistro`,`Apolice_idApolice`,`TipoSinistro_idTipoSinistro`),
  ADD KEY `fk_Sinistro_Apolice1_idx` (`Apolice_idApolice`),
  ADD KEY `fk_Sinistro_TipoSinistro1_idx` (`TipoSinistro_idTipoSinistro`);

--
-- Indexes for table `solicitacaoseguro`
--
ALTER TABLE `solicitacaoseguro`
  ADD PRIMARY KEY (`idSolicitacaoSeguro`),
  ADD KEY `fk_SolicitacaoSeguro_Candidato1_idx` (`Candidato_idCandidato`),
  ADD KEY `fk_SolicitacaoSeguro_Corretor1_idx` (`Corretor_idCorretor`),
  ADD KEY `fk_SolicitacaoSeguro_ItemProblemaSaude1_idx` (`ItemProblemaSaude_idItemProblemaSaude`);

--
-- Indexes for table `tiposinistro`
--
ALTER TABLE `tiposinistro`
  ADD PRIMARY KEY (`idTipoSinistro`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD UNIQUE KEY `nomeUsuario` (`nomeUsuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apolice`
--
ALTER TABLE `apolice`
  MODIFY `idApolice` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `avaliador`
--
ALTER TABLE `avaliador`
  MODIFY `idAvaliador` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `beneficiario`
--
ALTER TABLE `beneficiario`
  MODIFY `idBeneficiario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `candidato`
--
ALTER TABLE `candidato`
  MODIFY `idCandidato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `corretor`
--
ALTER TABLE `corretor`
  MODIFY `idCorretor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `endereco`
--
ALTER TABLE `endereco`
  MODIFY `idEndereco` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `itemproblemasaude`
--
ALTER TABLE `itemproblemasaude`
  MODIFY `idItemProblemaSaude` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `idPessoa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `problemasaude`
--
ALTER TABLE `problemasaude`
  MODIFY `idProblemaSaude` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `relatomorte`
--
ALTER TABLE `relatomorte`
  MODIFY `idRelatoMorte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `segurado`
--
ALTER TABLE `segurado`
  MODIFY `idSegurado` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sinistro`
--
ALTER TABLE `sinistro`
  MODIFY `idSinistro` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `solicitacaoseguro`
--
ALTER TABLE `solicitacaoseguro`
  MODIFY `idSolicitacaoSeguro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tiposinistro`
--
ALTER TABLE `tiposinistro`
  MODIFY `idTipoSinistro` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `apolice`
--
ALTER TABLE `apolice`
  ADD CONSTRAINT `fk_Apolice_SolicitacaoSeguro1` FOREIGN KEY (`SolicitacaoSeguro_idSolicitacaoSeguro`) REFERENCES `solicitacaoseguro` (`idSolicitacaoSeguro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `avaliador`
--
ALTER TABLE `avaliador`
  ADD CONSTRAINT `fk_Avaliador_Usuario1` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `beneficiario`
--
ALTER TABLE `beneficiario`
  ADD CONSTRAINT `Candidato_idCandidato` FOREIGN KEY (`Candidato_idCandidato`) REFERENCES `candidato` (`idCandidato`);

--
-- Limitadores para a tabela `candidato`
--
ALTER TABLE `candidato`
  ADD CONSTRAINT `fk_Candidato_Avaliador1` FOREIGN KEY (`Avaliador_idAvaliador`) REFERENCES `avaliador` (`idAvaliador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Candidato_Endereco1` FOREIGN KEY (`Endereco_idEndereco`) REFERENCES `endereco` (`idEndereco`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Candidato_Pessoa1` FOREIGN KEY (`Pessoa_idPessoa`) REFERENCES `pessoa` (`idPessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Candidato_Usuario1` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `corretor`
--
ALTER TABLE `corretor`
  ADD CONSTRAINT `fk_Corretor_Usuario1` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `segurado`
--
ALTER TABLE `segurado`
  ADD CONSTRAINT `fk_Segurado_Apolice1` FOREIGN KEY (`Apolice_idApolice`) REFERENCES `apolice` (`idApolice`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Segurado_Candidato1` FOREIGN KEY (`Candidato_idCandidato`) REFERENCES `candidato` (`idCandidato`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `sinistro`
--
ALTER TABLE `sinistro`
  ADD CONSTRAINT `fk_Sinistro_Apolice1` FOREIGN KEY (`Apolice_idApolice`) REFERENCES `apolice` (`idApolice`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Sinistro_TipoSinistro1` FOREIGN KEY (`TipoSinistro_idTipoSinistro`) REFERENCES `tiposinistro` (`idTipoSinistro`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `solicitacaoseguro`
--
ALTER TABLE `solicitacaoseguro`
  ADD CONSTRAINT `fk_SolicitacaoSeguro_Candidato1` FOREIGN KEY (`Candidato_idCandidato`) REFERENCES `candidato` (`idCandidato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_SolicitacaoSeguro_Corretor1` FOREIGN KEY (`Corretor_idCorretor`) REFERENCES `corretor` (`idCorretor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_SolicitacaoSeguro_ItemProblemaSaude1` FOREIGN KEY (`ItemProblemaSaude_idItemProblemaSaude`) REFERENCES `itemproblemasaude` (`idItemProblemaSaude`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
