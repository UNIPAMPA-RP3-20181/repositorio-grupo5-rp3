/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.Scanner;
import model.Avaliador;
import model.Candidato;
import model.Corretor;
import model.Endereco;
import model.PessoaInterface;
import model.Usuario;
import model.dao.DAOMestra;

/**
 *
 * @author Leonardo
 */
public class Teste {

    public static void main(String[] args) {
        int var = 0;
        int escolha = 0;
        while (var == 0) {
            System.out.println("Aperte 1 para cadastrar ou 2 para logar");
            Scanner input = new Scanner(System.in);
            escolha = input.nextInt();
            switch (escolha) {
                case 1: cadastro();
                case 2: login();
                
            }
        }
    }

    public static void cadastro() {
        Scanner input = new Scanner(System.in);
        System.out.println("Nome");
        String nome = input.nextLine();
        System.out.println("ocupacao");
        String ocupacao = input.nextLine();
        System.out.println("cpf");
        String cpf = input.nextLine();
        System.out.println("telefone");
        String telefone = input.nextLine();
        System.out.println("estadocivil");
        String estadoCivil = input.nextLine();
        System.out.println("sexo");
        String sexo = input.nextLine();
        System.out.println("estado");
        String estado = input.nextLine();
        System.out.println("cep");
        String cep = input.nextLine();
        System.out.println("cidade");
        String cidade = input.nextLine();
        System.out.println("bairro");
        String bairro = input.nextLine();
        System.out.println("rua");
        String rua = input.nextLine();
        System.out.println("nomelogin");
        String nomeLogin = input.nextLine();
        System.out.println("email");
        String email = input.nextLine();
        System.out.println("senha");
        String senha = input.nextLine();
        System.out.println("datanasicmento");
        String dataNascimento = input.nextLine();

        Endereco end = new Endereco(rua, cep, bairro, cidade, estado);
        Usuario usuario = new Usuario(nomeLogin, senha, email);
        Candidato cand = new Candidato(usuario, ocupacao, nome, cpf, telefone, sexo, estadoCivil, dataNascimento, end);

        DAOMestra daoMestra = new DAOMestra();
        daoMestra.RegistrarCandidato(cand);
    }

    public static void login() {
        Scanner input = new Scanner(System.in);
        System.out.println("nomedelogin");
        String nomeLogin = input.nextLine();
        System.out.println("senhadelogin");
        String senhaLogin = input.nextLine();
        DAOMestra mestra = new DAOMestra();
        PessoaInterface p = mestra.validarValores(nomeLogin, senhaLogin);
        if (p instanceof Candidato) {
            System.out.println("É candidato");
        } else if (p instanceof Corretor){
            System.out.println("É corretor");
        } else if (p instanceof Avaliador){
            System.out.println("É avaliador");
        }
        System.out.println("acabo");
    }
}
