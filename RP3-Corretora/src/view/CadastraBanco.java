/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import model.dao.ConexaoBanco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Candidato;
import model.Endereco;
import model.Usuario;

/**
 *
 * @author Leonardo
 */
public class CadastraBanco {

    public static void cadastrar(String nome, String cpf, String telefone,
            String sexo, String estadoCivil, Endereco end, Usuario user) throws Exception {
        /* Define a SQL */

 /* Abre a conexão que criamos o retorno é armazenado na variavel conn */
        Connection conn = ConexaoBanco.abrir();

        /* Mapeamento objeto relacional */
        String sql = "INSERT INTO endereco(RuaEndereco, CepEndereco, BairroEndereco, CidadeEndereco, EstadoEndereco)"
                + "VALUES ('" + end.getRua() + "', '" + end.getCep() + "', '" + end.getBairro() + "', '" + end.getCidade() + "', '" + end.getEstado() + "');\n";
        PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ResultSet rs = stmt.getGeneratedKeys();
        String sql2 = "INSERT INTO usuario(nomeUsuario, senhaUsuario, emailUsuario)"
                + "VALUES ('" + user.getNomeLogin() + "', '" + user.getSenhaLogin() + "', '" + user.getEmailCadastro() + "');\n";
        PreparedStatement stmt2 = conn.prepareStatement(sql2, Statement.RETURN_GENERATED_KEYS);
        ResultSet rs2 = stmt2.getGeneratedKeys();
        String sql3 = "INSERT INTO pessoa(nomePessoa, cpfPessoa, telefonePessoa, sexoPessoa, estadoCivilPessoa, Endereco_idEndereco, Usuario_idUsuario)"
                + "VALUES ('" + nome + "', '" + cpf + "', '" + telefone + "', '" + sexo + "', '" + estadoCivil + "', '" + stmt.RETURN_GENERATED_KEYS + "', '" + stmt2.RETURN_GENERATED_KEYS + "');";
        PreparedStatement stmt3 = conn.prepareStatement(sql3);

        /* Executa a SQL e captura o resultado da consulta */
        stmt.execute();
        stmt2.execute();
        stmt3.execute();

        stmt.close();
        stmt2.close();
        stmt3.close();
        conn.close();


        /* Retorna a lista contendo o resultado da consulta */
    }

    /**
    public Pessoa login(String username, String password) {
        Pessoa pessoa;
        Endereco endereco;
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * from pessoa, endereco, usuario WHERE pessoa.Endereco_idEndereco = endereco.idEndereco and pessoa.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=? and usuario.senhaUsuario=?");
            pst.setString(1, username);
            pst.setString(2, password);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                pessoa = new Pessoa();
                endereco = new Endereco();
                pessoa.setNome(rs.getString("nomePessoa"));
                pessoa.setCpf(rs.getString("cpfPessoa"));
                pessoa.setTelefone(rs.getString("telefonePessoa"));
                pessoa.setSexo(rs.getString("sexoPessoa"));
                pessoa.setEstadoCivil(rs.getString("estadoCivilPessoa"));
                endereco.setRua(rs.getString("RuaEndereco"));
                endereco.setCep(rs.getString("CepEndereco"));
                endereco.setBairro(rs.getString("BairroEndereco"));
                endereco.setCidade(rs.getString("CidadeEndereco"));
                endereco.setEstado(rs.getString("EstadoEndereco"));
                pessoa.setEnd(endereco);
                return pessoa;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        endereco = null;
        return pessoa = null;
    }
    */

    public ArrayList<Candidato> pegaTodosCandidatos() {
        try {
            Connection conn = ConexaoBanco.abrir();
            String sql = "SELECT * FROM pessoa";

            PreparedStatement stmt = null;
            ResultSet rs = null;

            ArrayList<Candidato> candidatos = new ArrayList<>();

            try {
                stmt = conn.prepareStatement(sql);
                rs = stmt.executeQuery();

                while (rs.next()) {

                    Usuario user = new Usuario("NomeLogin", "Senha", "Email");
                    Endereco end = new Endereco("Rua", "Cep", "Bairro", "Cidade", "Estado");
                    Candidato candidato = new Candidato(user, "as", "aa", "12", "12", "aa", "12", "AS", end);
                    candidato.setOcupacao(rs.getString("ocupacao"));
                    candidato.setNome(rs.getString("nome"));
                    candidato.setCpf((rs.getString("cpf")));
                    candidato.setTelefone((rs.getString("telefone")));
                    candidato.setSexo(rs.getString("sexo"));
                    candidato.setEstadoCivil(rs.getString("estadocivil"));
                    candidatos.add(candidato);
                }

            } catch (SQLException ex) {
                System.err.println("Erro ao criar lista de todos candidatos no banco: " + ex);
            } finally {
                stmt.close();
                conn.close();
                rs.close();
            }
            return candidatos;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
