/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author roliv
 */
public class Usuario {

    protected String nomeLogin;
    protected String senhaLogin;
    protected String emailCadastro;

    public Usuario(String nomeLogin, String senhaLogin, String emailCadastro) {
        this.nomeLogin = nomeLogin;
        this.senhaLogin = senhaLogin;
        this.emailCadastro = emailCadastro;
    }

    public Usuario() {
    }

    public String getNomeLogin() {
        return nomeLogin;
    }

    public void setNomeLogin(String nomeLogin) {
        this.nomeLogin = nomeLogin;
    }

    public String getSenhaLogin() {
        return senhaLogin;
    }

    public void setSenhaLogin(String senhaLogin) {
        this.senhaLogin = senhaLogin;
    }

    public String getEmailCadastro() {
        return emailCadastro;
    }

    public void setEmailCadastro(String emailCadastro) {
        this.emailCadastro = emailCadastro;
    }

}
