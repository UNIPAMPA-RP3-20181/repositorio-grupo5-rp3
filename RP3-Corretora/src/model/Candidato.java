/**
 * Package
 */
package model;

/**
 * Import
 */
import java.util.ArrayList;
import java.util.List;

/**
 * @author Rodrigo, LeonardoS
 */
public class Candidato extends Pessoa implements PessoaInterface{
    
    private Usuario usuario; 
    private List<Beneficiario> listaBeneficiario;
    private String ocupacao;

    public Candidato(Usuario usuario, String ocupacao, String nome, String cpf, String telefone, String sexo,
            String estadoCivil, String dataNascimento, Endereco end) {
        super(nome, cpf, telefone, sexo, estadoCivil, dataNascimento, end);
        this.usuario = usuario;
        this.listaBeneficiario = new <Beneficiario>ArrayList();
        this.ocupacao = ocupacao;
    }

    public Candidato() {}

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public String getOcupacao() {
        return ocupacao;
    }

    public void setOcupacao(String ocupacao) {
        this.ocupacao = ocupacao;
    }
    
    public List<Beneficiario> getListaBeneficiario(){
        return this.listaBeneficiario;
    }
    
    public void setListaBeneficiario(List<Beneficiario> listaDeBene){
        this.listaBeneficiario = listaDeBene;
    }
    
    public void addBeneficiario(Beneficiario beneficiario){
        this.listaBeneficiario.add(beneficiario);
    }
    
    @Deprecated
    public void removeBeneficiario(Beneficiario beneficiario){
        for (Beneficiario bene: listaBeneficiario) {
            if(bene.equals(beneficiario)){
                this.listaBeneficiario.remove(bene);
            }
        }

    }
    
    public boolean contains(String pesquisa) {
        return (this.nome.toUpperCase()).contains(pesquisa.toUpperCase());
    }

    

}
