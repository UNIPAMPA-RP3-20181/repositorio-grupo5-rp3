/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Calendar;

/**
 *
 * @author roliv
 * @author Leonardo
 * @email leopedroso45@gmail.com
 *
 */
public class Apolice {

    private SolicitacaoSeguro solicitacao;
    private String tipoSegurado;
    private Calendar dataEmissao;
    private double premio;
    private double valorSinistroMorte;
    private double valorSinistroIncapacitado;
    private int qntParcelas;
    private double valorParcelas;
    private int dataPagamento;
    private int numeroCartao;
    private Calendar dataVencimento;
    private String nomeDonoCartao;
    private String bandeira;
    private String banco;
    private String cpfDono;

    public Apolice(SolicitacaoSeguro solicitacao, String tipoSegurado, Calendar dataEmissao, double premio,
            double valorSinistroMorte, double valorSinistroIncapacitado, int qntParcelas, double valorParcelas,
            int dataPagamento, int numeroCartao, Calendar dataVencimento, String nomeDonoCartao, String bandeira,
            String banco, String cpfDono) {
        this.solicitacao = solicitacao;
        this.tipoSegurado = tipoSegurado;
        this.dataEmissao = dataEmissao;
        this.premio = premio;
        this.valorSinistroMorte = valorSinistroMorte;
        this.valorSinistroIncapacitado = valorSinistroIncapacitado;
        this.qntParcelas = qntParcelas;
        this.valorParcelas = valorParcelas;
        this.dataPagamento = dataPagamento;
        this.numeroCartao = numeroCartao;
        this.dataVencimento = dataVencimento;
        this.nomeDonoCartao = nomeDonoCartao;
        this.bandeira = bandeira;
        this.banco = banco;
        this.cpfDono = cpfDono;
    }

    public SolicitacaoSeguro getSolicitacao() {
        return solicitacao;
    }

    public void setSolicitacao(SolicitacaoSeguro solicitacao) {
        this.solicitacao = solicitacao;
    }

    public String getTipoSegurado() {
        return tipoSegurado;
    }

    public void setTipoSegurado(String tipoSegurado) {
        this.tipoSegurado = tipoSegurado;
    }

    public Calendar getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Calendar dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public double getPremio() {
        return premio;
    }

    public void setPremio(double premio) {
        this.premio = premio;
    }

    public double getValorSinistroMorte() {
        return valorSinistroMorte;
    }

    public void setValorSinistroMorte(double valorSinistroMorte) {
        this.valorSinistroMorte = valorSinistroMorte;
    }

    public double getValorSinistroIncapacitado() {
        return valorSinistroIncapacitado;
    }

    public void setValorSinistroIncapacitado(double valorSinistroIncapacitado) {
        this.valorSinistroIncapacitado = valorSinistroIncapacitado;
    }

    public int getQntParcelas() {
        return qntParcelas;
    }

    public void setQntParcelas(int qntParcelas) {
        this.qntParcelas = qntParcelas;
    }

    public double getValorParcelas() {
        return valorParcelas;
    }

    public void setValorParcelas(double valorParcelas) {
        this.valorParcelas = valorParcelas;
    }

    public int getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(int dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public int getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(int numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public Calendar getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Calendar dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public String getNomeDonoCartao() {
        return nomeDonoCartao;
    }

    public void setNomeDonoCartao(String nomeDonoCartao) {
        this.nomeDonoCartao = nomeDonoCartao;
    }

    public String getBandeira() {
        return bandeira;
    }

    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getCpfDono() {
        return cpfDono;
    }

    public void setCpfDono(String cpfDono) {
        this.cpfDono = cpfDono;
    }

}
