/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author roliv
 * @author Leonardo
 * @email: leopedroso45@gmail.com
 */
public class Avaliador implements PessoaInterface{
    private String nome;
    private boolean ativo;
    private String telefone;
    private Usuario usuario;

    public Avaliador(String nome, boolean ativo, String telefone, Usuario usuario) {
        this.nome = nome;
        this.ativo = ativo;
        this.telefone = telefone;
        this.usuario = usuario;
    }

    public Avaliador() {}

    
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    } 

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    
    
}
