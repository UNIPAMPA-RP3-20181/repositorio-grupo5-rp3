/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author roliv
 * @author Leonardo
 * @email leopedroso45@gmail.com
 */
public class Corretor implements PessoaInterface {

    private String nome;
    private String dataContratacao;
    private boolean ativoCorretor;
    private Usuario usuario;

    public Corretor(String dataContratacao, boolean ativoCorretor, String nomeLogin, String senhaLogin,
            String emailCadastro) {
        this.dataContratacao = dataContratacao;
        this.ativoCorretor = ativoCorretor;
    }

    public Corretor() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getDataContratacao() {
        return dataContratacao;
    }

    public void setDataContratacao(String dataContratacao) {
        this.dataContratacao = dataContratacao;
    }

    public boolean isAtivoCorretor() {
        return ativoCorretor;
    }

    public void setAtivoCorretor(boolean ativoCorretor) {
        this.ativoCorretor = ativoCorretor;
    }
}
