/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author roliv
 */
public class OperacoesBeneficiario {
    
    protected List<Beneficiario> beneficiarios;
    
    public OperacoesBeneficiario(){
        this.beneficiarios = new ArrayList();
    }
    

    public void cadastrarBeneficiario(Beneficiario beneficiario) throws IllegalArgumentException, NumberFormatException {
        if (!this.beneficiarios.add(beneficiario)) {
            throw new IllegalArgumentException("Esse beneficiario não pôde ser inserido.\nVerifique os dados informados.");
        }
    }
    
    public boolean removerCandidato(String pesquisa) {
        for (Beneficiario beneficiario : beneficiarios) {
            if (beneficiario.contains(pesquisa)) {
                if (this.beneficiarios.remove(beneficiario)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean editarBeneficiario(String pesquisa, Beneficiario beneficiario) {
        for (int i = 0; i < beneficiarios.size(); i++) {
            Beneficiario beneficiarioTemp = beneficiarios.get(i);
            if (beneficiarioTemp.contains(pesquisa)) {
                this.beneficiarios.set(i, beneficiario);
                return true;
            }
        }
        return false;
    }
}
