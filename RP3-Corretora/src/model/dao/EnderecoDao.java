/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.swing.JOptionPane;
import model.Endereco;

/**
 *
 * @author Leonardo
 * @email leopedroso45@gmail.com
 *
 */
public class EnderecoDAO implements DAO<Endereco> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    @Override
    public void insert(Endereco obj) {
        Endereco end = obj;
        conectar();
        try {
            String sql = "INSERT INTO endereco(RuaEndereco, CepEndereco, BairroEndereco, CidadeEndereco, EstadoEndereco)"
                    + "VALUES ('" + end.getRua() + "', '" + end.getCep() + "', '" + end.getBairro() + "', '" + end.getCidade() + "', '" + end.getEstado() + "');\n";
            comando.executeUpdate(sql);
            System.out.println("Inserido com sucesso!");

        } catch (SQLException e) {
            imprimeErro("Erro ao inserir Endereco", e.getMessage());
        } finally {
            fechar();
        }
    }

    @Override
    public void remove(Endereco obj) {
        Endereco end = obj;
    }

    @Override
    public void update(Endereco obj) {
        Endereco end = obj;
    }

    @Override
    public Endereco select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void conectar() {
        try {
            con = ConFactory.conexao(URL, USUARIO, SENHA, ConFactory.MYSQL);
            comando = con.createStatement();
            System.out.println("Conectado!");
        } catch (ClassNotFoundException e) {
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

    private void fechar() {
        try {
            comando.close();
            con.close();
            System.out.println("Conexão Fechada");
        } catch (SQLException e) {
            imprimeErro("Erro ao fechar conexão", e.getMessage());
        }
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }

}
