/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;

import java.util.List;
import javax.swing.JOptionPane;

import model.Candidato;

/**
 *
 * @author Leonardo
 * @email leopedroso45@gmail.com
 *
 */
public class CandidatoDAO implements DAO<Candidato> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    @Override
    public void insert(Candidato candidato) {
        Candidato cand = candidato;
        int acessoDeCandidato = 1;
        conectar();
        try {
            String sql = "INSERT INTO candidato(acesso, nomeCandidato, ocupacaoCandidato, cpfCandidato, telefoneCandidato, sexoCandidato, estadoCivilCandidato, Endereco_idEndereco, Usuario_idUsuario)"
                    + "VALUES ('" + acessoDeCandidato + "', '" + cand.getNome() + "', '" + cand.getOcupacao() + "', '" + cand.getCpf() + "', '" + cand.getTelefone() + "', '" + cand.getSexo() + "', '" + cand.getEstadoCivil() + "', (SELECT endereco.idEndereco FROM endereco ORDER BY endereco.idEndereco DESC LIMIT 1)"
                    + " ,(SELECT usuario.idUsuario FROM usuario ORDER BY usuario.idUsuario DESC LIMIT 1))";
            comando.executeUpdate(sql);
            System.out.println("Inserido com sucesso!");
        } catch (SQLException e) {
            imprimeErro("Erro ao inserir Pessoa", e.getMessage());
        } finally {
            fechar();
        }
    }

    @Override
    public Candidato select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Candidato> selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(Candidato candidato) {
    }

    @Override
    public void update(Candidato candidato) {
    }

    private void conectar() {
        try {
            con = ConFactory.conexao(URL, USUARIO, SENHA, ConFactory.MYSQL);
            comando = con.createStatement();
            System.out.println("Conectado!");
        } catch (ClassNotFoundException e) {
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

    private void fechar() {
        try {
            comando.close();
            con.close();
            System.out.println("Conexão Fechada");
        } catch (SQLException e) {
            imprimeErro("Erro ao fechar conexão", e.getMessage());
        }
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }
}
