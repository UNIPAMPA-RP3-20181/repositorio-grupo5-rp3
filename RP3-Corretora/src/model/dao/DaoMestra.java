/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import model.Avaliador;

import model.Candidato;
import model.Corretor;
import model.Endereco;
import model.PessoaInterface;
import model.Usuario;

/**
 * @param <T>
 * @author Leonardo
 * @email leopedroso45@gmail.com
 *
 */
public class DAOMestra<T> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    public DAOMestra() {
    }

    public void RegistrarCandidato(Candidato candidato) {
        CandidatoDAO daoCandidato = new CandidatoDAO();
        EnderecoDAO daoEndereco = new EnderecoDAO();
        UsuarioDAO daoUsuario = new UsuarioDAO();
        try {
            daoEndereco.insert(candidato.getEnd());
            daoUsuario.insert(candidato.getUsuario());
            daoCandidato.insert(candidato);
        } catch (Exception e) {
            e.getMessage();
        } finally {
            daoCandidato = null;
            daoEndereco = null;
            daoUsuario = null;
        }
    }

    public PessoaInterface validarValores(String username, String password) {
        PessoaInterface individuo;
        if (username == null) {
            JOptionPane.showMessageDialog(null, "Invalid user name!");
        } else if (password == null) {
            JOptionPane.showMessageDialog(null, "Invalid password!");
        } else {
            try {
                int acesso = DescobrirAcesso(username, password);
                switch (acesso) {
                    case 1:
                        individuo = loginCandidato(username, password);
                        return individuo;
                    case 2:
                        individuo = loginCorretor(username, password);
                        return individuo;
                    case 3:
                        individuo = loginAvaliador(username, password);
                        return individuo;
                    default:
                        break;
                }
            } catch (Exception ex) {
                imprimeErro("Erro ao descobrir o acesso", ex.getMessage());
            }
        }
        imprimeErro("Erro ao descobrir o acesso", "Erro fora do else");
        return individuo = null;
    }

    private Candidato loginCandidato(String username, String password) {
        Candidato cand;
        Endereco end;
        Usuario usu;
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * from candidato, endereco, usuario WHERE candidato.Endereco_idEndereco = endereco.idEndereco and candidato.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=? and usuario.senhaUsuario=?");
            pst.setString(1, username);
            pst.setString(2, password);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                cand = new Candidato();
                end = new Endereco();
                usu = new Usuario();
                cand.setNome(rs.getString("nomeCandidato"));
                cand.setOcupacao(rs.getString("ocupacaoCandidato"));
                cand.setCpf(rs.getString("cpfCandidato"));
                cand.setTelefone(rs.getString("telefoneCandidato"));
                cand.setSexo(rs.getString("sexoCandidato"));
                cand.setEstadoCivil(rs.getString("estadoCivilCandidato"));
                end.setRua(rs.getString("RuaEndereco"));
                end.setCep(rs.getString("CepEndereco"));
                end.setBairro(rs.getString("BairroEndereco"));
                end.setCidade(rs.getString("CidadeEndereco"));
                end.setEstado(rs.getString("EstadoEndereco"));
                cand.setEnd(end);
                usu.setEmailCadastro(rs.getString("emailUsuario"));
                usu.setNomeLogin(rs.getString("nomeUsuario"));
                usu.setSenhaLogin(rs.getString("senhaUsuario"));
                cand.setUsuario(usu);
                return cand;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        end = null;
        usu = null;
        return cand = null;
    }

    private Corretor loginCorretor(String username, String password) {
        Corretor cor;
        Usuario usu;
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * from corretor, usuario WHERE corretor.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=? and usuario.senhaUsuario=?");
            pst.setString(1, username);
            pst.setString(2, password);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                cor = new Corretor();
                usu = new Usuario();
                cor.setNome(rs.getString("nomeCorretor"));
                cor.setAtivoCorretor(rs.getBoolean("AtivoCorretor"));
                usu.setEmailCadastro(rs.getString("emailUsuario"));
                usu.setNomeLogin(rs.getString("nomeUsuario"));
                usu.setSenhaLogin(rs.getString("senhaUsuario"));
                cor.setUsuario(usu);
                return cor;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        usu = null;
        return cor = null;
    }
    
    private Avaliador loginAvaliador(String username, String password) {
        Avaliador ava;
        Usuario usu;
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * from avaliador, usuario WHERE avaliador.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=? and usuario.senhaUsuario=?");
            pst.setString(1, username);
            pst.setString(2, password);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                ava = new Avaliador();
                usu = new Usuario();
                ava.setNome(rs.getString("nomeAvaliador"));
                ava.setAtivo(rs.getBoolean("AtivoAvaliador"));
                ava.setTelefone(rs.getString("telefone"));
                usu.setEmailCadastro(rs.getString("emailUsuario"));
                usu.setNomeLogin(rs.getString("nomeUsuario"));
                usu.setSenhaLogin(rs.getString("senhaUsuario"));
                ava.setUsuario(usu);
                return ava;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        usu = null;
        return ava = null;
    }

    private int DescobrirAcesso(String username, String password) throws Exception {
        int acesso = 0;
        try {
            Connection conn = ConexaoBanco.abrir();
            String sql = "SELECT candidato.acesso from candidato, usuario WHERE candidato.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=  ?  and usuario.senhaUsuario=  ? ";
            String sql2 = "SELECT avaliador.acesso from avaliador, usuario WHERE avaliador.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=  ?  and usuario.senhaUsuario=  ? ";
            String sql3 = "SELECT corretor.acesso from corretor, usuario WHERE corretor.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=  ?  and usuario.senhaUsuario=  ?  ";
            PreparedStatement pst = conn.prepareStatement(sql);
            PreparedStatement pst2 = conn.prepareStatement(sql2);
            PreparedStatement pst3 = conn.prepareStatement(sql3);
            pst.setString(1, username);
            pst.setString(2, password);
            pst2.setString(1, username);
            pst2.setString(2, password);
            pst3.setString(1, username);
            pst3.setString(2, password);
            ResultSet rs = pst.executeQuery();
            ResultSet rs2 = pst2.executeQuery();
            ResultSet rs3 = pst3.executeQuery();
            while (rs.next()) {
                acesso = rs.getInt("acesso");
                return acesso;
            }
            while (rs2.next()) {
                acesso = rs2.getInt("acesso");
                return acesso;
            }
            while (rs3.next()) {
                acesso = rs3.getInt("acesso");
                return acesso;
            }
        } catch (SQLException e) {
            imprimeErro("Erro ao descobrir o acesso", e.getMessage());
        }
        System.err.println("");
        return acesso;
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }
}
