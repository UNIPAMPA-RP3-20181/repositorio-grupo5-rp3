/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.swing.JOptionPane;
import model.Usuario;

/**
 *
 * @author Leonardo
 * @email leopedroso45@gmail.com
 *
 */
public class UsuarioDAO implements DAO<Usuario> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    @Override
    public void insert(Usuario obj) {

        Usuario cand = obj;
        conectar();
        try {
            String sql = "INSERT INTO usuario(nomeUsuario, senhaUsuario, emailUsuario)"
                    + "VALUES ('" + cand.getNomeLogin() + "', '" + cand.getSenhaLogin() + "', '" + cand.getEmailCadastro() + "');";
            comando.executeUpdate(sql);
            System.out.println("Inserido com sucesso!");

        } catch (SQLException e) {
            imprimeErro("Erro ao inserir Usuario", e.getMessage());
        } finally {
            fechar();
        }
    }

    @Override
    public void remove(Usuario obj) {
        Usuario cand = obj;
    }

    @Override
    public void update(Usuario obj) {
        Usuario cand = obj;
    }

    @Override
    public Usuario select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void conectar() {
        try {
            con = ConFactory.conexao(URL, USUARIO, SENHA, ConFactory.MYSQL);
            comando = con.createStatement();
            System.out.println("Conectado!");
        } catch (ClassNotFoundException e) {
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

    private void fechar() {
        try {
            comando.close();
            con.close();
            System.out.println("Conexão Fechada");
        } catch (SQLException e) {
            imprimeErro("Erro ao fechar conexão", e.getMessage());
        }
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }

}
