/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Leonardo
 */
public class OperacoesCandidato {
    protected List<Candidato> candidatos;
    
    public OperacoesCandidato(){
        this.candidatos = new ArrayList();
    }
    

    public void cadastrarCandidato(Candidato candidato) throws IllegalArgumentException, NumberFormatException {
        if (!this.candidatos.add(candidato)) {
            throw new IllegalArgumentException("Esse Candidato não pôde ser inserido.\nVerifique os dados informados.");
        }
    }
    
    public boolean removerCandidato(String pesquisa) {
        for (Candidato candidato : candidatos) {
            if (candidato.contains(pesquisa)) {
                if (this.candidatos.remove(candidato)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean editarCandidato(String pesquisa, Candidato candidato) {
        for (int i = 0; i < candidatos.size(); i++) {
            Candidato candidatoTemp = candidatos.get(i);
            if (candidatoTemp.contains(pesquisa)) {
                this.candidatos.set(i, candidato);
                return true;
            }
        }
        return false;
    }
}
