# README #

This README contains some interesting links and the business rules of the proposed system.

### Sistema de Controle de Seguros de Vida - UniPampa ###

### Important Links ###

* https://moodle.unipampa.edu.br/moodle/pluginfile.php/314319/mod_resource/content/1/MD-050_APPLICATION_EXTENSIONS_FUNCTIONAL_DESIGN%20%20-%20Seguradora%20de%20Vida%20v1_2.pdf
* https://bitbucket.org/UNIPAMPA-RP3-20181/repositorio-grupo5-rp3

### Business Rules ###

* A solicitação de um seguro exige que o candidato tenha cadastradado ao menos um beneficiário;
* A solicitação de um seguro implica também na produção de uma simulação do valor do seguro. Esta simulação é temporária e poderá ser alterada de acordo com avaliação do avaliador.
* Uma avaliação de solicitação de seguro pode recusar um pedido de seguro de vida;
* Caso um pedido seja aprovado, deve-se agendar uma entrevista com o candidato ao seguro para determinar se a sua saúde está de acordo com o que foi preenchido pelo candidato no formulário de solicitação. Este agendamento inclui a solicitação de uma série de exames que o candidato deverá fazer;
* Ao contratar um seguro, o candidato passa automaticamente a ser um segurado cliente da empresa e seu cadastro deverá ser atualizado como tal;
* O processo de contratação de seguro implica em informar um cartão de crédito e em escolher a quantidade de parcelas que serão geradas.
* O segurado pode parcelar o pagamento do prêmio em até doze parcelas. 
* A notificação de um sinistro por um segurado implica a ocorrência de um acidente ou doença que incapacite/invalide o segurado permanentemente;
* Os sinistros só serão atendidos se o segurado estiver em dia com as parcelas do seguro.
* Um seguro e sua apólice têm validade de um ano a partir da data de contratação.
* Não serão aprovados seguros de vida para candidatos com menos de 18 anos.
* Não serão aprovados seguros de vida para candidatos com mais de 90 anos.
* Não serão aprovados seguros de vida para candidatos que respondam positivamente a quatro ou mais predisposições às doenças registradas.
* Não serão aprovados seguros de vida para candidatos que respondam positivamente que são fumantes e alcoolistas e que respondam positivamente a duas ou mais predisposições às doenças registradas.
* O banco de dados deve manter um histórico de alterações na solicitação de seguro de forma que seja possível saber os valores informados pelo candidato e os valores corrigidos pelo avaliador.