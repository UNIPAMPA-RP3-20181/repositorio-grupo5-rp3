/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguro;

/**
 *
 * @author roliv
 */
public class Diagnostico {
    private ProblemaSaude problema;
    private String resultado;
    private String observacao;

    public Diagnostico(ProblemaSaude problema, String resultado, String observacao) {
        this.problema = problema;
        this.resultado = resultado;
        this.observacao = observacao;
    }

    public ProblemaSaude getProblema() {
        return problema;
    }

    public void setProblema(ProblemaSaude problema) {
        this.problema = problema;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    
}
