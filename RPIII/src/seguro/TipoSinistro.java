/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguro;

/**
 *
 * @author roliv
 */
public class TipoSinistro {
    private String tipo;
    private boolean fatalidade;

    public TipoSinistro(String tipo, boolean fatalidade) {
        this.tipo = tipo;
        this.fatalidade = fatalidade;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isFatalidade() {
        return fatalidade;
    }

    public void setFatalidade(boolean fatalidade) {
        this.fatalidade = fatalidade;
    }
    
    
}
