/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguro;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author roliv
 */
public class ConexaoBanco {
    
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String url = "jdbc:mysql://localhost:3306/corretora";
    private static final String user = "root";
    private static final String pass = "";
    
//    public static void main(String[] args) throws Exception {
//        getConnection();
//    }
    
    public static Connection abrir() throws Exception{
        try{
            Class.forName(driver);
            
            Connection conn = DriverManager.getConnection(url, user, pass);
            System.out.println("Connected");
            return conn;
        }catch(Exception e){
            System.out.println(e);
        }
        
        return null;
    }
    
}
