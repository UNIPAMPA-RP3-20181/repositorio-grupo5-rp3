/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguro;

/**
 *
 * @author roliv
 */
public class Apolice {
    private SolicitacaoSeguro solicitacao;
    private CartaoCredito cartao;
    private String tipoSegurado;
    private int dataEmissao;
    private double premio;
    private double valorSinistroMorte;
    private double valorSinistroIncapacitado;
    private int qntParcelas;
    private double valorParcelas;
    private int dataPagamento;

    public Apolice(SolicitacaoSeguro solicitacao, CartaoCredito cartao, String tipoSegurado,
            int dataEmissao, double premio, double valorSinistroMorte, double valorSinistroIncapacitado,
            int qntParcelas, double valorParcelas, int dataPagamento) {
        this.solicitacao = solicitacao;
        this.cartao = cartao;
        this.tipoSegurado = tipoSegurado;
        this.dataEmissao = dataEmissao;
        this.premio = premio;
        this.valorSinistroMorte = valorSinistroMorte;
        this.valorSinistroIncapacitado = valorSinistroIncapacitado;
        this.qntParcelas = qntParcelas;
        this.valorParcelas = valorParcelas;
        this.dataPagamento = dataPagamento;
    }

    public SolicitacaoSeguro getSolicitacao() {
        return solicitacao;
    }

    public void setSolicitacao(SolicitacaoSeguro solicitacao) {
        this.solicitacao = solicitacao;
    }

    public CartaoCredito getCartao() {
        return cartao;
    }

    public void setCartao(CartaoCredito cartao) {
        this.cartao = cartao;
    }

    public String getTipoSegurado() {
        return tipoSegurado;
    }

    public void setTipoSegurado(String tipoSegurado) {
        this.tipoSegurado = tipoSegurado;
    }

    public int getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(int dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public double getPremio() {
        return premio;
    }

    public void setPremio(double premio) {
        this.premio = premio;
    }

    public double getValorSinistroMorte() {
        return valorSinistroMorte;
    }

    public void setValorSinistroMorte(double valorSinistroMorte) {
        this.valorSinistroMorte = valorSinistroMorte;
    }

    public double getValorSinistroIncapacitado() {
        return valorSinistroIncapacitado;
    }

    public void setValorSinistroIncapacitado(double valorSinistroIncapacitado) {
        this.valorSinistroIncapacitado = valorSinistroIncapacitado;
    }

    public int getQntParcelas() {
        return qntParcelas;
    }

    public void setQntParcelas(int qntParcelas) {
        this.qntParcelas = qntParcelas;
    }

    public double getValorParcelas() {
        return valorParcelas;
    }

    public void setValorParcelas(double valorParcelas) {
        this.valorParcelas = valorParcelas;
    }

    public int getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(int dataPagamento) {
        this.dataPagamento = dataPagamento;
    }
    
    
}
