/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguro;

/**
 *
 * @author roliv
 */
public class Corretor extends Usuario{
    private int dataContratacao;
    private boolean ativoCorretor;

    public Corretor(int dataContratacao, boolean ativoCorretor, String nomeLogin, String senhaLogin,
            String emailCadastro) {
        super(nomeLogin, senhaLogin, emailCadastro);
        this.dataContratacao = dataContratacao;
        this.ativoCorretor = ativoCorretor;
    }

    public int getDataContratacao() {
        return dataContratacao;
    }

    public void setDataContratacao(int dataContratacao) {
        this.dataContratacao = dataContratacao;
    }

    public boolean isAtivoCorretor() {
        return ativoCorretor;
    }

    public void setAtivoCorretor(boolean ativoCorretor) {
        this.ativoCorretor = ativoCorretor;
    }

    public String getNomeLogin() {
        return nomeLogin;
    }

    public void setNomeLogin(String nomeLogin) {
        this.nomeLogin = nomeLogin;
    }

    public String getSenhaLogin() {
        return senhaLogin;
    }

    public void setSenhaLogin(String senhaLogin) {
        this.senhaLogin = senhaLogin;
    }

    public String getEmailCadastro() {
        return emailCadastro;
    }

    public void setEmailCadastro(String emailCadastro) {
        this.emailCadastro = emailCadastro;
    }
    
    
}
