/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguro;

/**
 *
 * @author roliv
 */
public class Sinistro {
    private Avaliador avaliador;
    private Apolice apolice;
    private TipoSinistro tipoSinistro;
    private int data;
    private String descricao;
    private double valorSinistro;
    private boolean autorizadoSinistro;
    private String parecerAvaliador;
    private String contatoSinistro;
    private int cpfContato;

    public Sinistro(Avaliador avaliador, Apolice apolice, TipoSinistro tipoSinistro, int data,
            String descricao, double valorSinistro, boolean autorizadoSinistro, String parecerAvaliador,
            String contatoSinistro, int cpfContato) {
        this.avaliador = avaliador;
        this.apolice = apolice;
        this.tipoSinistro = tipoSinistro;
        this.data = data;
        this.descricao = descricao;
        this.valorSinistro = valorSinistro;
        this.autorizadoSinistro = autorizadoSinistro;
        this.parecerAvaliador = parecerAvaliador;
        this.contatoSinistro = contatoSinistro;
        this.cpfContato = cpfContato;
    }

    public Avaliador getAvaliador() {
        return avaliador;
    }

    public void setAvaliador(Avaliador avaliador) {
        this.avaliador = avaliador;
    }

    public Apolice getApolice() {
        return apolice;
    }

    public void setApolice(Apolice apolice) {
        this.apolice = apolice;
    }

    public TipoSinistro getTipoSinistro() {
        return tipoSinistro;
    }

    public void setTipoSinistro(TipoSinistro tipoSinistro) {
        this.tipoSinistro = tipoSinistro;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValorSinistro() {
        return valorSinistro;
    }

    public void setValorSinistro(double valorSinistro) {
        this.valorSinistro = valorSinistro;
    }

    public boolean isAutorizadoSinistro() {
        return autorizadoSinistro;
    }

    public void setAutorizadoSinistro(boolean autorizadoSinistro) {
        this.autorizadoSinistro = autorizadoSinistro;
    }

    public String getParecerAvaliador() {
        return parecerAvaliador;
    }

    public void setParecerAvaliador(String parecerAvaliador) {
        this.parecerAvaliador = parecerAvaliador;
    }

    public String getContatoSinistro() {
        return contatoSinistro;
    }

    public void setContatoSinistro(String contatoSinistro) {
        this.contatoSinistro = contatoSinistro;
    }

    public int getCpfContato() {
        return cpfContato;
    }

    public void setCpfContato(int cpfContato) {
        this.cpfContato = cpfContato;
    }
    
    
}