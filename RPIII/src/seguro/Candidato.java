/**
 * Package
 */
package seguro;

/**
 * Import
 */
import java.util.ArrayList;
import java.util.List;

/**
 * @author Rodrigo, LeonardoS
 */
public class Candidato extends Pessoa {

    private Usuario usuario;
    private List<Beneficiario> beneficiario;
    private String ocupacao;

    public Candidato(Usuario usuario, String ocupacao, String nome, String cpf, String telefone, String sexo,
            String estadoCivil, Endereco end) {
        super(nome, cpf, telefone, sexo, estadoCivil, end);
        //this.diagnostico = diagnostico;
        this.usuario = usuario;
        //this.avaliador = avaliador;
        this.beneficiario = new <Beneficiario>ArrayList();
        this.ocupacao = ocupacao;
    }


    /**
     * public Diagnostico getDiagnostico() { return diagnostico; }
     *
     *
     * public void setDiagnostico(Diagnostico diagnostico) { this.diagnostico =
     * diagnostico; }
     */

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * public Avaliador getAvaliador() { return avaliador; }
     *
     * public void setAvaliador(Avaliador avaliador) { this.avaliador =
     * avaliador; }
     */

    public String getOcupacao() {
        return ocupacao;
    }

    public void setOcupacao(String ocupacao) {
        this.ocupacao = ocupacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Endereco getEnd() {
        return end;
    }

    public void setEnd(Endereco end) {
        this.end = end;
    }

    public boolean contains(String pesquisa) {
        return (this.nome.toUpperCase()).contains(pesquisa.toUpperCase());
    }

    @Override
    public String toString() {
        return "Candidato{" + "usuario=" + usuario + ", beneficiario=" + beneficiario + ", ocupacao=" + ocupacao + '}';
    }

}
