/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguro;

/**
 *
 * @author roliv
 */
public class CartaoCredito {
    private int numeroCartao;
    private int dataVencimento;
    private String nomeCartao;
    private int cfpDono;
    private String bandeiraCartao;
    private String nomeBanco;

    public CartaoCredito(int numeroCartao, int dataVencimento, String nomeCartao, int cfpDono,
            String bandeiraCartao, String nomeBanco) {
        this.numeroCartao = numeroCartao;
        this.dataVencimento = dataVencimento;
        this.nomeCartao = nomeCartao;
        this.cfpDono = cfpDono;
        this.bandeiraCartao = bandeiraCartao;
        this.nomeBanco = nomeBanco;
    }

    public int getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(int numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public int getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(int dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public String getNomeCartao() {
        return nomeCartao;
    }

    public void setNomeCartao(String nomeCartao) {
        this.nomeCartao = nomeCartao;
    }

    public int getCfpDono() {
        return cfpDono;
    }

    public void setCfpDono(int cfpDono) {
        this.cfpDono = cfpDono;
    }

    public String getBandeiraCartao() {
        return bandeiraCartao;
    }

    public void setBandeiraCartao(String bandeiraCartao) {
        this.bandeiraCartao = bandeiraCartao;
    }

    public String getNomeBanco() {
        return nomeBanco;
    }

    public void setNomeBanco(String nomeBanco) {
        this.nomeBanco = nomeBanco;
    }
    
    
}
