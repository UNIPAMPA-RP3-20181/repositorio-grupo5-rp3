/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguro;

/**
 *
 * @author roliv
 */
public class Beneficiario extends Pessoa {
    private String email;
    private String parentesco;

    public Beneficiario(String nome, String cpf, String telefone, String sexo, String estadoCivil,
            int dataNascimento, Endereco end, String email, String parentesco) {
        super (nome, cpf, telefone, sexo, estadoCivil, end);
        this.email = email;
        this.parentesco = parentesco;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Endereco getEnd() {
        return end;
    }

    public void setEnd(Endereco end) {
        this.end = end;
    }

    public boolean contains(String pesquisa) {
        return (this.nome.toUpperCase()).contains(pesquisa.toUpperCase());
    }
}
