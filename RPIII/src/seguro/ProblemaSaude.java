/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguro;

/**
 *
 * @author roliv
 */
public class ProblemaSaude {
    private String nomeProbSaude;
    private String descricao;

    public ProblemaSaude(String nomeProbSaude, String descricao) {
        this.nomeProbSaude = nomeProbSaude;
        this.descricao = descricao;
    }

    public String getNomeProbSaude() {
        return nomeProbSaude;
    }

    public void setNomeProbSaude(String nomeProbSaude) {
        this.nomeProbSaude = nomeProbSaude;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    
}
