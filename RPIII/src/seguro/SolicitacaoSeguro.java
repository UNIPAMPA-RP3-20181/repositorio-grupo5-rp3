/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguro;

/**
 *
 * @author roliv
 */
public class SolicitacaoSeguro {
    private Candidato canditado;
    private Corretor corretor;
    private int sataSolicitacao;
    private double valorSolicitacao;
    private String statusSolicitacao;
    private String motivoReprovacao;
    private double valorCorrigido;
    private int predispCancer;
    private int predispDiabetes;
    private int predispDemencia;
    private int predispCoracao;
    private int predispCerebral;
    private int predispHipertensao;
    private int predispPulmonar;
    private int predispOsteoporose;
    private int predispDegeneracao;
    private boolean alcoolatra;
    private boolean fumante;
    
    /** atributos removidos do candidato
    private Diagnostico diagnostico;
    private Avaliador avaliador;
    */

    public SolicitacaoSeguro(Candidato canditado, Corretor corretor, int sataSolicitacao,
            double valorSolicitacao, String statusSolicitacao, String motivoReprovacao,
            double valorCorrigido, int predispCancer, int predispDiabetes, int predispDemencia,
            int predispCoracao, int predispCerebral, int predispHipertensao, int predispPulmonar,
            int predispOsteoporose, int predispDegeneracao, boolean alcoolatra, boolean fumante) {
        this.canditado = canditado;
        this.corretor = corretor;
        this.sataSolicitacao = sataSolicitacao;
        this.valorSolicitacao = valorSolicitacao;
        this.statusSolicitacao = statusSolicitacao;
        this.motivoReprovacao = motivoReprovacao;
        this.valorCorrigido = valorCorrigido;
        this.predispCancer = predispCancer;
        this.predispDiabetes = predispDiabetes;
        this.predispDemencia = predispDemencia;
        this.predispCoracao = predispCoracao;
        this.predispCerebral = predispCerebral;
        this.predispHipertensao = predispHipertensao;
        this.predispPulmonar = predispPulmonar;
        this.predispOsteoporose = predispOsteoporose;
        this.predispDegeneracao = predispDegeneracao;
        this.alcoolatra = alcoolatra;
        this.fumante = fumante;
    }

    public Candidato getCanditado() {
        return canditado;
    }

    public void setCanditado(Candidato canditado) {
        this.canditado = canditado;
    }

    public Corretor getCorretor() {
        return corretor;
    }

    public void setCorretor(Corretor corretor) {
        this.corretor = corretor;
    }

    public int getSataSolicitacao() {
        return sataSolicitacao;
    }

    public void setSataSolicitacao(int sataSolicitacao) {
        this.sataSolicitacao = sataSolicitacao;
    }

    public double getValorSolicitacao() {
        return valorSolicitacao;
    }

    public void setValorSolicitacao(double valorSolicitacao) {
        this.valorSolicitacao = valorSolicitacao;
    }

    public String getStatusSolicitacao() {
        return statusSolicitacao;
    }

    public void setStatusSolicitacao(String statusSolicitacao) {
        this.statusSolicitacao = statusSolicitacao;
    }

    public String getMotivoReprovacao() {
        return motivoReprovacao;
    }

    public void setMotivoReprovacao(String motivoReprovacao) {
        this.motivoReprovacao = motivoReprovacao;
    }

    public double getValorCorrigido() {
        return valorCorrigido;
    }

    public void setValorCorrigido(double valorCorrigido) {
        this.valorCorrigido = valorCorrigido;
    }

    public int isPredispCancer() {
        return predispCancer;
    }

    public void setPredispCancer(int predispCancer) {
        this.predispCancer = predispCancer;
    }

    public int isPredispDiabetes() {
        return predispDiabetes;
    }

    public void setPredispDiabetes(int predispDiabetes) {
        this.predispDiabetes = predispDiabetes;
    }

    public int isPredispDemencia() {
        return predispDemencia;
    }

    public void setPredispDemencia(int predispDemencia) {
        this.predispDemencia = predispDemencia;
    }

    public int isPredispCoracao() {
        return predispCoracao;
    }

    public void setPredispCoracao(int predispCoracao) {
        this.predispCoracao = predispCoracao;
    }

    public int isPredispCerebral() {
        return predispCerebral;
    }

    public void setPredispCerebral(int predispCerebral) {
        this.predispCerebral = predispCerebral;
    }

    public int isPredispHipertensao() {
        return predispHipertensao;
    }

    public void setPredispHipertensao(int predispHipertensao) {
        this.predispHipertensao = predispHipertensao;
    }

    public int isPredispPulmonar() {
        return predispPulmonar;
    }

    public void setPredispPulmonar(int predispPulmonar) {
        this.predispPulmonar = predispPulmonar;
    }

    public int isPredispOsteoporose() {
        return predispOsteoporose;
    }

    public void setPredispOsteoporose(int predispOsteoporose) {
        this.predispOsteoporose = predispOsteoporose;
    }

    public int isPredispDegeneracao() {
        return predispDegeneracao;
    }

    public void setPredispDegeneracao(int predispDegeneracao) {
        this.predispDegeneracao = predispDegeneracao;
    }

    public boolean isAlcoolatra() {
        return alcoolatra;
    }

    public void setAlcoolatra(boolean alcoolatra) {
        this.alcoolatra = alcoolatra;
    }

    public boolean isFumante() {
        return fumante;
    }

    public void setFumante(boolean fumante) {
        this.fumante = fumante;
    }
    
    
}
