/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author roliv
 */
public class Solicitacao {

    private double valor;
    private String statusCorretor;
    private String motivoReprovacaoCorretor;
    private String statusAvaliador;
    private String motivoReprovacaoAvaliador;
    private int valorCorrigido;
    private int preCancer;
    private int preDiabetes;
    private int preDemencia;
    private int preCoracao;
    private int preCerebral;
    private int preDegeneracao;
    private int prePulmonar;
    private boolean alcoolatra;
    private boolean fumante;
    private ItemProblemaSaude itens;
    private int idCandidato;
    private String dataSolicitacao;

    public Solicitacao(double valor, int staCorretor, String motivoReprovacaoCorretor, int staAvaliador, String motivoReprovacaoAvaliador, int valorCorrigido, int preCancer, int preDiabetes, int preDemencia, int preCoracao, int preCerebral, int preDegeneracao, int prePulmonar, boolean alcoolatra, boolean fumante, ItemProblemaSaude itens, int idCandidato, String dataSolicitacao) {
        this.valor = valor;
        switch (staCorretor) {
            case 0:
                this.statusCorretor = "Recusado";
                break;
            case 1:
                this.statusCorretor = "Aprovado";
                break;
            default:
                this.statusCorretor = "Pendente";
                break;
        }
        this.motivoReprovacaoCorretor = motivoReprovacaoCorretor;
        switch (staAvaliador) {
            case 0:
                this.statusAvaliador = "Recusado";
                break;
            case 1:
                this.statusAvaliador = "Aprovado";
                break;
            default:
                this.statusAvaliador = "Pendente";
                break;
        }
        this.motivoReprovacaoAvaliador = motivoReprovacaoAvaliador;
        this.valorCorrigido = valorCorrigido;
        this.preCancer = preCancer;
        this.preDiabetes = preDiabetes;
        this.preDemencia = preDemencia;
        this.preCoracao = preCoracao;
        this.preCerebral = preCerebral;
        this.preDegeneracao = preDegeneracao;
        this.prePulmonar = prePulmonar;
        this.alcoolatra = alcoolatra;
        this.fumante = fumante;
        this.itens = itens;
        this.idCandidato = idCandidato;
        this.dataSolicitacao = dataSolicitacao;
    }

    public String getStatusCorretor() {
        return statusCorretor;
    }

    public void setStatusCorretor(int statusCorretor) {
        if (statusCorretor == 0) {
            this.statusCorretor = "Recusado";
        } else if (statusCorretor == 1) {
            this.statusCorretor = "Aprovado";
        }else {
            this.statusCorretor = "Pendente";
        }

    }

    public String getStatusAvaliador() {
        return statusAvaliador;
    }

    public void setStatusAvaliador(int statusAvaliador) {
        if (statusAvaliador == 0) {
            this.statusAvaliador = "Recusado";
        } else if (statusAvaliador == 1) {
            this.statusAvaliador = "Aprovado";
        }else{
            this.statusAvaliador = "Pendente";
        }
    }

    public int getIdCandidato() {
        return idCandidato;
    }

    public void setIdCandidato(int idCandidato) {
        this.idCandidato = idCandidato;
    }

    public Solicitacao() {
    }

    public String getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(String dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getMotivoReprovacaoCorretor() {
        return motivoReprovacaoCorretor;
    }

    public void setMotivoReprovacaoCorretor(String motivoReprovacaoCorretor) {
        this.motivoReprovacaoCorretor = motivoReprovacaoCorretor;
    }

    public String getMotivoReprovacaoAvaliador() {
        return motivoReprovacaoAvaliador;
    }

    public void setMotivoReprovacaoAvaliador(String motivoReprovacaoAvaliador) {
        this.motivoReprovacaoAvaliador = motivoReprovacaoAvaliador;
    }

    public int getValorCorrigido() {
        return valorCorrigido;
    }

    public void setValorCorrigido(int valorCorrigido) {
        this.valorCorrigido = valorCorrigido;
    }

    public int getPreCancer() {
        return preCancer;
    }

    public void setPreCancer(int preCancer) {
        this.preCancer = preCancer;
    }

    public int getPreDiabetes() {
        return preDiabetes;
    }

    public void setPreDiabetes(int preDiabetes) {
        this.preDiabetes = preDiabetes;
    }

    public int getPreDemencia() {
        return preDemencia;
    }

    public void setPreDemencia(int preDemencia) {
        this.preDemencia = preDemencia;
    }

    public int getPreCoracao() {
        return preCoracao;
    }

    public void setPreCoracao(int preCoracao) {
        this.preCoracao = preCoracao;
    }

    public int getPreCerebral() {
        return preCerebral;
    }

    public void setPreCerebral(int preCerebral) {
        this.preCerebral = preCerebral;
    }

    public int getPreDegeneracao() {
        return preDegeneracao;
    }

    public void setPreDegeneracao(int preDegeneracao) {
        this.preDegeneracao = preDegeneracao;
    }

    public int getPrePulmonar() {
        return prePulmonar;
    }

    public void setPrePulmonar(int prePulmonar) {
        this.prePulmonar = prePulmonar;
    }

    public int isAlcoolatra() {
        if (alcoolatra == true) {
            return 1;
        } else {
            return 0;
        }
    }

    public void setAlcoolatra(boolean alcoolatra) {
        this.alcoolatra = alcoolatra;
    }

    public int isFumante() {
        if (fumante == true) {
            return 1;
        } else {
            return 0;
        }
    }

    public void setFumante(boolean fumante) {
        this.fumante = fumante;
    }

    public ItemProblemaSaude getItens() {
        return itens;
    }

    public void setItens(ItemProblemaSaude itens) {
        this.itens = itens;
    }

}
