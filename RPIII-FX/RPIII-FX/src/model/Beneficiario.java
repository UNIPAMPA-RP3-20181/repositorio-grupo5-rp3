/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author roliv
 * @author Leonardo
 * @email leopedroso45@gmail.com
 *
 */
public class Beneficiario implements PessoaInterface {

    private String nome;
    private String email;
    private String parentesco;
    private String contato;
    private int idCandidato;
    private int idBene;

    public Beneficiario(String nome, String email, String parentesco, String contato, int idCandidato, int idBene) {
        this.nome = nome;
        this.email = email;
        this.parentesco = parentesco;
        this.contato = contato;
        this.idCandidato = idCandidato;
        this.idBene = idBene;
                
    }

    public Beneficiario() {
    }

    public int getIdBene() {
        return idBene;
    }

    public void setIdBene(int idBene) {
        this.idBene = idBene;
    }

    public int getIdCandidato() {
        return idCandidato;
    }

    public void setIdCandidato(int idCandidato) {
        this.idCandidato = idCandidato;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public boolean contains(String pesquisa) {
        return (this.nome.toUpperCase()).contains(pesquisa.toUpperCase());
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.nome);
        hash = 97 * hash + Objects.hashCode(this.email);
        hash = 97 * hash + Objects.hashCode(this.parentesco);
        hash = 97 * hash + Objects.hashCode(this.contato);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Beneficiario other = (Beneficiario) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.parentesco, other.parentesco)) {
            return false;
        }
        if (!Objects.equals(this.contato, other.contato)) {
            return false;
        }
        return true;
    }

}
