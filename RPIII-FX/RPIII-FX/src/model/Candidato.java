/**
 * Package
 */
package model;

/**
 * Import
 */
import java.util.List;
import javafx.scene.image.Image;

/**
 * @author Rodrigo, LeonardoS
 */
public class Candidato extends Pessoa implements PessoaInterface{
    
    private Usuario usuario; 
    private List<Beneficiario> listaBeneficiario;
    private String ocupacao;
    private Image image;
    private int id;

    public Candidato(Usuario usuario, List<Beneficiario> listaBeneficiario, String ocupacao, Image image, String nome, String cpf, String telefone, String celular, String genero, String estadoCivil, String dataNascimento, Endereco end) {
        super(nome, cpf, telefone, celular, genero, estadoCivil, dataNascimento, end);
        this.usuario = usuario;
        this.listaBeneficiario = listaBeneficiario;
        this.ocupacao = ocupacao;
        this.image = image;
    }    

    public Candidato() {}

    public Usuario getUsuario() {
        return usuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Beneficiario> getListaBeneficiario() {
        return listaBeneficiario;
    }

    public void setListaBeneficiario(List<Beneficiario> listaBeneficiario) {
        this.listaBeneficiario = listaBeneficiario;
    }

    public String getOcupacao() {
        return ocupacao;
    }

    public void setOcupacao(String ocupacao) {
        this.ocupacao = ocupacao;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String getCpf() {
        return cpf;
    }

    @Override
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Override
    public String getTelefone() {
        return telefone;
    }

    @Override
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public String getCelular() {
        return celular;
    }

    @Override
    public void setCelular(String celular) {
        this.celular = celular;
    }

    @Override
    public String getGenero() {
        return genero;
    }

    @Override
    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public String getEstadoCivil() {
        return estadoCivil;
    }

    @Override
    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    @Override
    public String getDataNascimento() {
        return dataNascimento;
    }

    @Override
    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @Override
    public Endereco getEnd() {
        return end;
    }

    @Override
    public void setEnd(Endereco end) {
        this.end = end;
    }

    

    
    
    public void addBeneficiario(Beneficiario beneficiario){
        this.listaBeneficiario.add(beneficiario);
    }
    
    @Deprecated
    public void removeBeneficiario(Beneficiario beneficiario){
        for (Beneficiario bene: listaBeneficiario) {
            if(bene.equals(beneficiario)){
                this.listaBeneficiario.remove(bene);
            }
        }

    }
    
    public boolean contains(String pesquisa) {
        return (this.nome.toUpperCase()).contains(pesquisa.toUpperCase());
    }

    

}
