package model.dao;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Leonardo
 */
public class ConexaoBanco {

    private static final String USUARIO = "root";
    private static final String SENHA = "";
    private static final String URL = "jdbc:mysql://localhost:3306/corretora";
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    //Conectando ao banco
    public static Connection abrir() throws Exception {
        // Registrar o driver
        Class.forName(DRIVER);
        // Pegando conexão
        Connection conn = DriverManager.getConnection(URL, USUARIO, SENHA);
        // Retorna a conexao aberta
        return conn;
    }
    /**
     * public int login(String username, String password) { Pessoa pessoa; try {
     * Connection conn = ConexaoBanco.abrir(); PreparedStatement pst =
     * conn.prepareStatement("SELECT * from pessoa, endereco, usuario WHERE
     * pessoa.Endereco_idEndereco = endereco.idEndereco and
     * pessoa.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=?
     * and usuario.senhaUsuario=?"); pst.setString(1, username);
     * pst.setString(2, password); ResultSet rs = pst.execsuteQuery(); if
     * (rs.next()) { pessoa = new Pessoa(); endereco = new Endereco();
     * pessoa.setNome(rs.getString("nomePessoa"));
     * pessoa.setCpf(rs.getString("cpfPessoa"));
     * pessoa.setTelefone(rs.getString("telefonePessoa"));
     * pessoa.setSexo(rs.getString("sexoPessoa"));
     * pessoa.setEstadoCivil(rs.getString("estadoCivilPessoa"));
     * endereco.setRua(rs.getString("RuaEndereco"));
     * endereco.setCep(rs.getString("CepEndereco"));
     * endereco.setBairro(rs.getString("BairroEndereco"));
     * endereco.setCidade(rs.getString("CidadeEndereco"));
     * endereco.setEstado(rs.getString("EstadoEndereco"));
     * pessoa.setEnd(endereco); return pessoa; } } catch (Exception e) {
     * e.printStackTrace(); } endereco = null; return pessoa = null; }
     */

}
