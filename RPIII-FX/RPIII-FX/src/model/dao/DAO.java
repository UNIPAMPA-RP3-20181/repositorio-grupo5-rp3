/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;

/**
 *
 * @param <T>
 * @author Leonardo
 * @email leopedroso45@gmail.com
 * 
 */
public interface DAO<T> {
    
    public boolean insert(T obj);
    public void remove(int id);
    public void update(T obj, int id);
    public T select();
    public <T> List<T> selectAll();
    
    
}
