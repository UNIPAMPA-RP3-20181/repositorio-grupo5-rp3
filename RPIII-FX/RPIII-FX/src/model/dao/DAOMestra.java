/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import model.Avaliador;
import model.Beneficiario;

import model.Candidato;
import model.Corretor;
import model.Endereco;
import model.Solicitacao;
import model.Usuario;

/**
 * @param <T>
 * @author Leonardo
 * @email leopedroso45@gmail.com
 *
 */
public class DAOMestra<T> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    public DAOMestra() {
    }

    public boolean RegistrarCandidato(Candidato candidato) {
        CandidatoDAO daoCandidato = new CandidatoDAO();
        EnderecoDAO daoEndereco = new EnderecoDAO();
        UsuarioDAO daoUsuario = new UsuarioDAO();
        PessoaDAO daoPessoa = new PessoaDAO();
        try {
            if (daoEndereco.insert(candidato.getEnd())
                    && daoUsuario.insert(candidato.getUsuario())
                    && daoPessoa.insert(candidato)
                    && daoCandidato.insert(candidato)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.getMessage();
            return false;
        } finally {
            daoCandidato = null;
            daoEndereco = null;
            daoUsuario = null;
        }
    }

    public boolean atualizarCandidato(Candidato candidato, int id) {
        CandidatoDAO daoCandidato = new CandidatoDAO();
        EnderecoDAO daoEndereco = new EnderecoDAO();
        PessoaDAO daoPessoa = new PessoaDAO();
        try {
            daoEndereco.update(candidato.getEnd(), getIdBanco(id, "Endereco"));
            daoPessoa.update(candidato, getIdBanco(id, "Pessoa"));
            daoCandidato.update(candidato, id);
            return true;

        } catch (Exception e) {
            e.getMessage();
            return false;
        } finally {
            daoCandidato = null;
            daoEndereco = null;
        }
    }
    
    public boolean atualizarUsuario(Usuario user, int id) {
        UsuarioDAO daoUsuario = new UsuarioDAO();
        try {
            daoUsuario.update(user, getIdBanco(id, "Usuario"));
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;
        } finally {
            daoUsuario = null;
        }
    }

    public boolean RegistrarSolicitacao(Solicitacao sol) {
        SolicitacaoDAO daoSol = new SolicitacaoDAO();
        ItemProblemaSaudeDAO daoItem = new ItemProblemaSaudeDAO();
        ProblemaSaudeDAO daoProblema = new ProblemaSaudeDAO();
        try {
            if (daoItem.insert(sol.getItens())) {
                for (int i = 0; i < sol.getItens().getProblemas().size(); i++) {
                    daoProblema.insert(sol.getItens().getProblemas().get(i));
                }
            }
            daoSol.insert(sol);
            return true;
        } catch (Exception e) {
            e.getMessage();
            return false;
        } finally {
            daoSol = null;
            daoItem = null;
            daoProblema = null;
        }
    }

    public boolean RegistrarBeneficiario(Beneficiario beneficiario) {
        BeneficiarioDAO daoCBeneficiario = new BeneficiarioDAO();

        try {
            if (daoCBeneficiario.insert(beneficiario)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.getMessage();
            return false;
        } finally {
            daoCBeneficiario = null;
        }
    }

    public int validarValores(String username, String password) {
        int acess;
        if (username == null) {
            JOptionPane.showMessageDialog(null, "Invalid user name!");
        } else if (password == null) {
            JOptionPane.showMessageDialog(null, "Invalid password!");
        } else {
            try {
                int acesso = DescobrirAcesso(username, password);
                switch (acesso) {
                    case 1:
                        acess = 1;
                        return acess;
                    case 2:
                        acess = 2;
                        return acess;
                    case 3:
                        acess = 3;
                        return acess;
                    default:
                        break;
                }
            } catch (Exception ex) {
                imprimeErro("Erro ao descobrir o acesso", ex.getMessage());
            }
        }
        JOptionPane.showMessageDialog(null, "Nome de Usuário ou senha incorretos!");
        return 0;
    }

    public Candidato loginCandidato(String username, String password) {
        Candidato cand;
        Endereco end;
        Usuario usu;
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * from candidato, endereco, usuario, pessoa WHERE candidato.Endereco_idEndereco = endereco.idEndereco and candidato.Usuario_idUsuario = usuario.idUsuario and candidato.Pessoa_idPessoa = idPessoa and usuario.nomeUsuario=? and usuario.senhaUsuario=?");
            pst.setString(1, username);
            pst.setString(2, password);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                cand = new Candidato();
                end = new Endereco();
                usu = new Usuario();
                cand.setId(rs.getInt("idCandidato"));
                cand.setNome(rs.getString("nomePessoa"));
                cand.setOcupacao(rs.getString("ocupacaoCandidato"));
                cand.setCpf(rs.getString("cpfPessoa"));
                cand.setTelefone(rs.getString("telefonePessoa"));
                cand.setCelular(rs.getString("celularPessoa"));
                cand.setDataNascimento(rs.getString("dataPessoa"));
                cand.setGenero(rs.getString("sexoPessoa"));
                Blob blob = rs.getBlob("imagePerfil");
                if (blob != null) {
                    InputStream dados = blob.getBinaryStream(0, blob.length());
                    BufferedImage image = ImageIO.read(dados);
                    Image imagePerfil = SwingFXUtils.toFXImage(image, null);
                    cand.setImage(imagePerfil);
                }
                cand.setEstadoCivil(rs.getString("estadoCivilPessoa"));
                end.setRua(rs.getString("RuaEndereco"));
                end.setCep(rs.getString("CepEndereco"));
                end.setBairro(rs.getString("BairroEndereco"));
                end.setCidade(rs.getString("CidadeEndereco"));
                end.setEstado(rs.getString("EstadoEndereco"));
                end.setComplemento("Complemento");
                end.setNumero(rs.getString("NumeroEndereco"));
                cand.setEnd(end);
                usu.setEmailCadastro(rs.getString("emailUsuario"));
                usu.setNomeLogin(rs.getString("nomeUsuario"));
                usu.setSenhaLogin(rs.getString("senhaUsuario"));
                cand.setUsuario(usu);
                return cand;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        end = null;
        usu = null;
        return cand = null;
    }

    public Corretor loginCorretor(String username, String password) {
        Corretor cor;
        Usuario usu;
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * from corretor, usuario WHERE corretor.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=? and usuario.senhaUsuario=?");
            pst.setString(1, username);
            pst.setString(2, password);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                cor = new Corretor();
                usu = new Usuario();
                cor.setNome(rs.getString("nomeCorretor"));
                cor.setAtivoCorretor(rs.getBoolean("AtivoCorretor"));
                usu.setEmailCadastro(rs.getString("emailUsuario"));
                usu.setNomeLogin(rs.getString("nomeUsuario"));
                usu.setSenhaLogin(rs.getString("senhaUsuario"));
                cor.setUsuario(usu);
                return cor;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        usu = null;
        return cor = null;
    }

    private Avaliador loginAvaliador(String username, String password) {
        Avaliador ava;
        Usuario usu;
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * from avaliador, usuario WHERE avaliador.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=? and usuario.senhaUsuario=?");
            pst.setString(1, username);
            pst.setString(2, password);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                ava = new Avaliador();
                usu = new Usuario();
                ava.setNome(rs.getString("nomeAvaliador"));
                ava.setAtivo(rs.getBoolean("AtivoAvaliador"));
                ava.setTelefone(rs.getString("telefone"));
                usu.setEmailCadastro(rs.getString("emailUsuario"));
                usu.setNomeLogin(rs.getString("nomeUsuario"));
                usu.setSenhaLogin(rs.getString("senhaUsuario"));
                ava.setUsuario(usu);
                return ava;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        usu = null;
        return ava = null;
    }

    private int DescobrirAcesso(String username, String password) throws Exception {
        int acesso = 0;
        try {
            Connection conn = ConexaoBanco.abrir();
            String sql = "SELECT candidato.acesso from candidato, usuario WHERE candidato.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=  ?  and usuario.senhaUsuario=  ? ";
            String sql2 = "SELECT avaliador.acesso from avaliador, usuario WHERE avaliador.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=  ?  and usuario.senhaUsuario=  ? ";
            String sql3 = "SELECT corretor.acesso from corretor, usuario WHERE corretor.Usuario_idUsuario = usuario.idUsuario and usuario.nomeUsuario=  ?  and usuario.senhaUsuario=  ?  ";
            PreparedStatement pst = conn.prepareStatement(sql);
            PreparedStatement pst2 = conn.prepareStatement(sql2);
            PreparedStatement pst3 = conn.prepareStatement(sql3);
            pst.setString(1, username);
            pst.setString(2, password);
            pst2.setString(1, username);
            pst2.setString(2, password);
            pst3.setString(1, username);
            pst3.setString(2, password);
            ResultSet rs = pst.executeQuery();
            ResultSet rs2 = pst2.executeQuery();
            ResultSet rs3 = pst3.executeQuery();
            while (rs.next()) {
                acesso = rs.getInt("acesso");
                return acesso;
            }
            while (rs2.next()) {
                acesso = rs2.getInt("acesso");
                return acesso;
            }
            while (rs3.next()) {
                acesso = rs3.getInt("acesso");
                return acesso;
            }
        } catch (SQLException e) {
            imprimeErro("Erro ao descobrir o acesso", e.getMessage());
        }
        System.err.println("");
        return acesso;
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }

    private int getIdBanco(int idCandidato, String tabela) throws Exception {
        int idPessoa;
        int idUsuario;
        int idEndereco;
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * from candidato, endereco, usuario, pessoa WHERE candidato.idCandidato = " + idCandidato + " and candidato.Endereco_idEndereco = endereco.idEndereco and candidato.Usuario_idUsuario = usuario.idUsuario and candidato.Pessoa_idPessoa = idPessoa");
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                idPessoa = (rs.getInt("idPessoa"));
                idUsuario = (rs.getInt("idUsuario"));
                idEndereco = (rs.getInt("idEndereco"));
                if (tabela.equals("Pessoa")) {
                    return idPessoa;
                } else if (tabela.equals("Usuario")) {
                    return idUsuario;
                } else if (tabela.equals("Endereco")) {
                    return idEndereco;
                } else {
                    return -1;
                }
            }
        } catch (SQLException e) {
            imprimeErro("Erro ao descobrir o acesso", e.getMessage());

        }
        return -1;
    }
}
