/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.swing.JOptionPane;
import model.ProblemaSaude;

/**
 *
 * @author roliv
 */
public class ProblemaSaudeDAO implements DAO<ProblemaSaude>{
    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    @Override
    public boolean insert(ProblemaSaude probSaude) {
        conectar();
        try {
            String sql = "INSERT INTO problemasaude (nomeProblemaSaude, descricaoProblemaSaude, idItemProblemaSaude)"
                    + "VALUES ('" + probSaude.getNomeProbSaude() + "', '" + probSaude.getDescricao() + "', (SELECT itemproblemasaude.ProblemaSaude_idProblemaSaude FROM itemproblemasaude ORDER BY itemproblemasaude.ProblemaSaude_idProblemaSaude DESC LIMIT 1))";

            comando.executeUpdate(sql);
            System.out.println("Inserido com sucesso!");
            fechar();
            return true;
        } catch (SQLException e) {
            imprimeErro("Erro ao inserir Problema Saude", e.getMessage());
            return false;
        } finally {
            fechar();
        }
    }

    @Override
    public void remove(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(ProblemaSaude obj, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ProblemaSaude select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> List<T> selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void conectar() {
        try {
            con = ConFactory.conexao(URL, USUARIO, SENHA, ConFactory.MYSQL);
            comando = con.createStatement();
            System.out.println("Conectado!");
        } catch (ClassNotFoundException e) {
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

    private void fechar() {
        try {
            comando.close();
            con.close();
            System.out.println("Conexão Fechada");
        } catch (SQLException e) {
            imprimeErro("Erro ao fechar conexão", e.getMessage());
        }
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }
    
    
    
}
