/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.swing.JOptionPane;
import model.Endereco;

/**
 *
 * @author Leonardo
 * @email leopedroso45@gmail.com
 *
 */
public class EnderecoDAO implements DAO<Endereco> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    @Override
    public boolean insert(Endereco end) {
        conectar();
        try {
            String sql = "INSERT INTO endereco(RuaEndereco, NumeroEndereco, CepEndereco, BairroEndereco, CidadeEndereco, EstadoEndereco, Complemento)"
                    + "VALUES ('" + end.getRua() + "', '" + end.getNumero() + "', '" + end.getCep() + "', '" + end.getBairro() + "', '" + end.getCidade() + "', '" + end.getEstado() + "', '" + end.getComplemento() + "');\n";
            comando.executeUpdate(sql);
            System.out.println("Inserido com sucesso!");
            fechar();
            return true;
        } catch (SQLException e) {
            imprimeErro("Erro ao inserir Endereco", e.getMessage());
            fechar();
            return false;
        } finally {
            fechar();
        }
    }

    @Override
    public void remove(int id) {

    }

    @Override
    public void update(Endereco end, int id) {
        conectar();
        try {
            //Connection conn = ConexaoBanco.abrir();
            String sql = "UPDATE endereco SET endereco.RuaEndereco  = '" + end.getRua() + "', "
                    + "endereco.NumeroEndereco = '" + end.getNumero() + "', "
                    + "endereco.CepEndereco = '" + end.getCep() + "', "
                    + "endereco.BairroEndereco = '" + end.getBairro() + "', "
                    + "endereco.CidadeEndereco = '" + end.getCidade() + "', "
                    + "endereco.EstadoEndereco = '" + end.getEstado() + "', "
                    + "endereco.Complemento   = '" + end.getComplemento() + "' "
                    + "WHERE endereco.idEndereco = " + id + " ;";
            comando.executeUpdate(sql);

            //PreparedStatement pst = conn.prepareStatement(sql);
            //ResultSet rs = pst.executeQuery();
        } catch (SQLException e) {
            imprimeErro("Erro ao atualizar Endereco", e.getMessage());
            fechar();
        } finally {
            fechar();
        }

    }

    @Override
    public Endereco select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void conectar() {
        try {
            con = ConFactory.conexao(URL, USUARIO, SENHA, ConFactory.MYSQL);
            comando = con.createStatement();
            System.out.println("Conectado!");
        } catch (ClassNotFoundException e) {
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

    private void fechar() {
        try {
            comando.close();
            con.close();
            System.out.println("Conexão Fechada");
        } catch (SQLException e) {
            imprimeErro("Erro ao fechar conexão", e.getMessage());
        }
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }

}
