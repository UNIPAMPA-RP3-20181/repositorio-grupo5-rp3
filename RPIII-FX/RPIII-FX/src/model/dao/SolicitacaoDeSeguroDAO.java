/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Candidato;
import model.Endereco;
import model.SolicitacaoSeguro;

/**
 *
 * @author Leonardo
 */
public class SolicitacaoDeSeguroDAO implements DAO<SolicitacaoSeguro> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    @Override
    public boolean insert(SolicitacaoSeguro solicitacao) {
        return false;
//        SolicitacaoSeguro sol = solicitacao;
//        int acessoDeCandidato = 1;
//        conectar();
//        try {
//            String sql = "INSERT INTO solicitacaoseguro(acesso, valorSolicitacaoSeguro, StatusSolicitacao, "
//                    + "MotivoReprovacaoSeguro, ValorCorrigidoSeguro, PredispCancerSeguro, PredispDiabeteSeguro,"
//                    + " PredispDemenciaSeguro, PredispCoracaoSeguro, PredispCerebralSeguro, PredispHipertensaoSeguro, PredispPulmonarSeguro, PredispOsteoporoseSeguro, PredispDegeneracaoSeguro, alcoolatra )"
//                    + "VALUES ('" + acessoDeCandidato + "', '" + cand.getNome() + "', '" + cand.getOcupacao() + "', '" + cand.getCpf() + "', '" + cand.getTelefone() + "', '" + cand.getGenero() + "', '" + cand.getEstadoCivil() + "', (SELECT endereco.idEndereco FROM endereco ORDER BY endereco.idEndereco DESC LIMIT 1)"
//                    + " ,(SELECT usuario.idUsuario FROM usuario ORDER BY usuario.idUsuario DESC LIMIT 1))";
//            comando.executeUpdate(sql);
//            System.out.println("Inserido com sucesso!");
//        } catch (SQLException e) {
//            imprimeErro("Erro ao inserir Pessoa", e.getMessage());
//        } finally {
//            fechar();
//        }
    }

    /**
     * Remover uma solicitaçãoDeSeguro
     *
     * @deprecated @param obj
     */
    @Override
    public void remove(int id) {
        try {
            Connection conn = ConexaoBanco.abrir();
            String sql = "DELETE FROM solicitacaoseguro WHERE solicitacaoseguro.idSolicitacaoSeguro = ? ;";
            conn.prepareStatement(sql);
        } catch (Exception e) {
            imprimeErro("Erro ao tentar excluir a solicitacao", e.getMessage());
        }
    }

    /**
     *
     * método que atualiza a solicitacao recebida
     *
     * @deprecated
     * @param obj
     */
    @Override
    public void update(SolicitacaoSeguro obj, int id) {
        SolicitacaoSeguro sS = obj;
        try {
            Connection conn = ConexaoBanco.abrir();
            String sql = "UPDATE solicitacaoseguro\n"
                    + "SET solicitacaoseguro.valorSolicitacaoSeguro = " + sS.getValorSolicitacao() + " ,"
                    + "solicitacaoseguro.StatusSolicitacao = " + sS.getStatusSolicitacao() + " ,"
                    + " solicitacaoseguro.MotivoReprovacaoSeguro = " + sS.getMotivoReprovacao() + " ,"
                    + "solicitacaoseguro.ValorCorrigidoSeguro = " + sS.getValorCorrigido() + " ,"
                    + "solicitacaoseguro.PredispCancerSeguro = " + sS.getPredispCancer() + " ,"
                    + "solicitacaoseguro.PredispDiabeteSeguro = " + sS.getPredispDiabetes() + " ,"
                    + "solicitacaoseguro.PredispDemenciaSeguro = " + sS.getPredispDemencia() + " ,"
                    + " solicitacaoseguro.PredispCoracaoSeguro = " + sS.getPredispCoracao() + " ,"
                    + "solicitacaoseguro.PredispCerebralSeguro = " + sS.getPredispCerebral() + " ,"
                    + "solicitacaoseguro.PredispHipertensaoSeguro = " + sS.getPredispHipertensao() + " ,"
                    + "solicitacaoseguro.PredispPulmonarSeguro = " + sS.getPredispPulmonar() + " ,"
                    + "solicitacaoseguro.PredispOsteoporoseSeguro = " + sS.getPredispOsteoporose() + " ,"
                    + "solicitacaoseguro.PredispDegeneracaoSeguro = " + sS.getPredispDegeneracao() + " ,"
                    + "solicitacaoseguro.alcoolatra = " + sS.isAlcoolatra() + " ,"
                    + "solicitacaoseguro.fumante = " + sS.isFumante() + "  "
                    + "WHERE solicitacaoseguro.idSolicitacaoSeguro = ? ;";
            PreparedStatement pst = conn.prepareStatement(sql);
            //ResultSet rs = pst.executeQuery();
        } catch (Exception e) {
            imprimeErro("Erro ao atualizar a solicitacao", e.getMessage());
        }

    }

    /**
     *
     * @return nada ainda
     */
    @Override
    public SolicitacaoSeguro select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @deprecated @param indice
     * @return SolicitaoSeguro tirar duvida sobre indice
     */
    public SolicitacaoSeguro selectInt(int indice) {

        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * FROM solicitacaoseguro,"
                    + " candidato, endereco"
                    + " WHERE solicitacaoseguro.Candidato_idCandidato = candidato.idCandidato"
                    + " AND"
                    + " candidato.Endereco_idEndereco = endereco.idEndereco");
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                SolicitacaoSeguro sS = new SolicitacaoSeguro();
                Candidato cand = new Candidato();
                Endereco end = new Endereco();

                sS.setValorSolicitacao(rs.getInt("valorSolicitacaoSeguro"));
                sS.setStatusSolicitacao(rs.getString("StatusSolicitacao"));
                sS.setMotivoReprovacao(rs.getString("MotivoReprovacaoSeguro"));
                sS.setValorCorrigido(rs.getInt("ValorCorrigidoSeguro"));
                sS.setPredispCancer(rs.getInt("PredispCancerSeguro"));
                sS.setPredispDiabetes(rs.getInt("PredispDiabeteSeguro"));
                sS.setPredispDemencia(rs.getInt("PredispDemenciaSeguro"));
                sS.setPredispCoracao(rs.getInt("PredispCoracaoSeguro"));
                sS.setPredispCerebral(rs.getInt("PredispCerebralSeguro"));
                sS.setPredispHipertensao(rs.getInt("PredispHipertensaoSeguro"));
                sS.setPredispPulmonar(rs.getInt("PredispPulmonarSeguro"));
                sS.setPredispOsteoporose(rs.getInt("PredispOsteoporoseSeguro"));
                sS.setPredispDegeneracao(rs.getInt("PredispDegeneracaoSeguro"));
                sS.setAlcoolatra(rs.getBoolean("alcoolatra"));
                sS.setFumante(rs.getBoolean("fumante"));

                cand.setOcupacao(rs.getString("ocupacaoCandidato"));
                cand.setNome(rs.getString("nomeCandidato"));
                cand.setCpf(rs.getString("cpfCandidato"));
                cand.setTelefone(rs.getString("telefoneCandidato"));
                cand.setGenero(rs.getString("sexoCandidato"));
                cand.setEstadoCivil(rs.getString("estadoCivilCandidato"));

                end.setRua(rs.getString("RuaEndereco"));
                end.setCep(rs.getString("CepEndereco"));
                end.setBairro(rs.getString("BairroEndereco"));
                end.setCidade(rs.getString("CidadeEndereco"));
                end.setEstado(rs.getString("EstadoEndereco"));

                cand.setEnd(end);
                sS.setCanditado(cand);
                return sS;
            }
        } catch (Exception e) {
            imprimeErro("Erro ao dar update na solicitao", e.getMessage());
        }
        return null;
    }

    /**
     *
     * Método que retorna uma lista com as solicitações do banco
     *
     * @return List<SolicitacaoSeguro>
     */
    @Override
    public List<SolicitacaoSeguro> selectAll() {
        List<SolicitacaoSeguro> lista = new ArrayList<>();
        SolicitacaoSeguro sS;
        Candidato cand;
        Endereco end;
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * from solicitacaoseguro \n"
                    + "INNER JOIN candidato on solicitacaoseguro.Candidato_idCandidato = candidato.idCandidato\n"
                    + "INNER JOIN endereco on Endereco_idEndereco = candidato.Endereco_idEndereco");
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                sS = new SolicitacaoSeguro();

                cand = new Candidato();
                end = new Endereco();

                sS.setValorSolicitacao(rs.getInt("valorSolicitacaoSeguro"));
                sS.setStatusSolicitacao(rs.getString("StatusSolicitacao"));
                sS.setMotivoReprovacao(rs.getString("MotivoReprovacaoSeguro"));
                sS.setValorCorrigido(rs.getInt("ValorCorrigidoSeguro"));
                sS.setPredispCancer(rs.getInt("PredispCancerSeguro"));
                sS.setPredispDiabetes(rs.getInt("PredispDiabeteSeguro"));
                sS.setPredispDemencia(rs.getInt("PredispDemenciaSeguro"));
                sS.setPredispCoracao(rs.getInt("PredispCoracaoSeguro"));
                sS.setPredispCerebral(rs.getInt("PredispCerebralSeguro"));
                sS.setPredispHipertensao(rs.getInt("PredispHipertensaoSeguro"));
                sS.setPredispPulmonar(rs.getInt("PredispPulmonarSeguro"));
                sS.setPredispOsteoporose(rs.getInt("PredispOsteoporoseSeguro"));
                sS.setPredispDegeneracao(rs.getInt("PredispDegeneracaoSeguro"));
                sS.setAlcoolatra(rs.getBoolean("alcoolatra"));
                sS.setFumante(rs.getBoolean("fumante"));

                cand.setOcupacao(rs.getString("ocupacaoCandidato"));
                cand.setNome(rs.getString("nomeCandidato"));
                cand.setCpf(rs.getString("cpfCandidato"));
                cand.setTelefone(rs.getString("telefoneCandidato"));
                cand.setGenero(rs.getString("sexoCandidato"));
                cand.setEstadoCivil(rs.getString("estadoCivilCandidato"));

                end.setRua(rs.getString("RuaEndereco"));
                end.setCep(rs.getString("CepEndereco"));
                end.setBairro(rs.getString("BairroEndereco"));
                end.setCidade(rs.getString("CidadeEndereco"));
                end.setEstado(rs.getString("EstadoEndereco"));

                cand.setEnd(end);
                sS.setCanditado(cand);
                lista.add(sS);
            }
        } catch (Exception e) {
            imprimeErro("Erro ao selecionar todas solicitacoes", e.getMessage());
        } finally {
            return lista;
        }
    }

    private void conectar() {
        try {
            con = ConFactory.conexao(URL, USUARIO, SENHA, ConFactory.MYSQL);
            comando = con.createStatement();
            System.out.println("Conectado!");
        } catch (ClassNotFoundException e) {
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

    private void fechar() {
        try {
            comando.close();
            con.close();
            System.out.println("Conexão Fechada");
        } catch (SQLException e) {
            imprimeErro("Erro ao fechar conexão", e.getMessage());
        }
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }

    private SolicitacaoSeguro SolicitacaoSeguro() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
