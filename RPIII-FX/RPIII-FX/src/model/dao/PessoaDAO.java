/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.List;
import javax.swing.JOptionPane;
import model.Candidato;

/**
 *
 * @author roliv
 */
public class PessoaDAO implements DAO<Candidato> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    @Override
    public boolean insert(Candidato candidato) {
        Candidato pess = candidato;
        conectar();
        try {
            String sql = "INSERT INTO pessoa(nomePessoa, cpfPessoa, telefonePessoa, celularPessoa, "
                    + "sexoPessoa, estadoCivilPessoa, dataPessoa )"
                    + "VALUES ('" + pess.getNome() + "', '" + pess.getCpf() + "', '" + pess.getTelefone()
                    + "', '" + pess.getCelular() + "', '" + pess.getGenero() + "', '" + pess.getEstadoCivil()
                    + "', '" + pess.getDataNascimento() + "');";
            comando.executeUpdate(sql);
            System.out.println("Inserido com sucesso!");
            fechar();
            return true;
        } catch (SQLException e) {
            if (e instanceof SQLIntegrityConstraintViolationException) {
                JOptionPane.showMessageDialog(null, "Este cpf já foi cadastrado!");
                fechar();
                return false;
            } else {
                imprimeErro("Erro ao inserir Pessoa", e.getMessage());
                fechar();
                return false;
            }
        }
    }

    @Override
    public void remove(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Candidato candidato, int id) {
        Candidato cand = candidato;
        conectar();
        try {
            //Connection conn = ConexaoBanco.abrir();
            String sql = "UPDATE pessoa\n"
                    + "SET pessoa.nomePessoa = '" + cand.getNome() + "' ,"
                    + "pessoa.telefonePessoa = '" + cand.getTelefone() + "' ,"
                    + "pessoa.celularPessoa = '" + cand.getCelular() + "' ,"
                    + "pessoa.sexoPessoa = '" + cand.getGenero() + "' ,"
                    + "pessoa.estadoCivilPessoa = '" + cand.getEstadoCivil() + "' ,"
                    + "pessoa.dataPessoa = '" + cand.getDataNascimento() + "'  "
                    + "WHERE pessoa.idPessoa = " + id + " ;";
            comando.executeUpdate(sql);
            //PreparedStatement pst = conn.prepareStatement(sql);
            //ResultSet rs = pst.executeQuery();
        } catch (SQLException e) {
            imprimeErro("Erro ao atualizar pessoa", e.getMessage());
            fechar();
        } finally {
            fechar();
        }
    }

    @Override
    public Candidato select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> List<T> selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void fechar() {
        try {
            comando.close();
            con.close();
            System.out.println("Conexão Fechada");
        } catch (SQLException e) {
            imprimeErro("Erro ao fechar conexão", e.getMessage());
        }
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }

    private void conectar() {
        try {
            con = ConFactory.conexao(URL, USUARIO, SENHA, ConFactory.MYSQL);
            comando = con.createStatement();
            System.out.println("Conectado!");
        } catch (ClassNotFoundException e) {
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

}
