/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.SQLException;

import java.util.List;
import javax.swing.JOptionPane;

import model.Candidato;

/**
 *
 * @author Leonardo
 * @email leopedroso45@gmail.com
 *
 */
public class CandidatoDAO implements DAO<Candidato> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    @Override
    public boolean insert(Candidato cand) {
        int acessoDeCandidato = 1;
        conectar();
        try {
            String sql = "INSERT INTO candidato(acesso, ocupacaoCandidato, Endereco_idEndereco, Usuario_idUsuario, Pessoa_idPessoa)"
                    + "VALUES ('" + acessoDeCandidato + "', '" + cand.getOcupacao() + "', "
                    + "(SELECT endereco.idEndereco FROM endereco ORDER BY endereco.idEndereco DESC LIMIT 1)"
                    + " ,(SELECT usuario.idUsuario FROM usuario ORDER BY usuario.idUsuario DESC LIMIT 1)"
                    + " ,(SELECT pessoa.idPessoa FROM pessoa ORDER BY pessoa.idPessoa DESC LIMIT 1))";
                    
            comando.executeUpdate(sql);
            System.out.println("Inserido com sucesso!");
            fechar();
            return true;
        } catch (SQLException e) {
            imprimeErro("Erro ao inserir Candidato", e.getMessage());
            return false;
        } finally {
            fechar();
            
        }
    }

    @Override
    public Candidato select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

//    public Candidato selectInt(int id) throws Exception {
//        Connection conn = ConexaoBanco.abrir();
//        java.sql.Statement stmt = null;
//        try {
//            String sql = "SELECT * FROM candidato where candidato.idCandidato = " + id + ";";
//            PreparedStatement pst = conn.prepareStatement(sql);
//            ResultSet rs = pst.executeQuery();
//            while (rs.next()) {
//                System.out.println("tá indo");
//                Candidato candidato = new Candidato(rs.getNString("nomeCandidato"));
//                return candidato;
//            }
//        } catch (SQLException e) {
//            imprimeErro("Erro ao recuperar candidato pelo id!", e.getMessage());
//        }
//        return null;
//    }

    @Override
    public List<Candidato> selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(int id) {
    }

    @Override
    public void update(Candidato cand, int id) {

        conectar();
        try {
            //Connection conn = ConexaoBanco.abrir();
            String sql = "UPDATE candidato\n"
                    + "SET candidato.ocupacaoCandidato = '" + cand.getOcupacao() + "' ,"
                    + "candidato.imagePerfil  = " + cand.getImage() + "  "
                    + "WHERE candidato.idCandidato = " + id + " ;";
            comando.executeUpdate(sql);
            //PreparedStatement pst = conn.prepareStatement(sql);
            //ResultSet rs = pst.executeQuery();
        } catch (SQLException e) {
            imprimeErro("Erro ao atualizar pessoa", e.getMessage());
            fechar();
        } finally {
            fechar();
        }
    }

    private void conectar() {
        try {
            con = ConFactory.conexao(URL, USUARIO, SENHA, ConFactory.MYSQL);
            comando = con.createStatement();
            System.out.println("Conectado!");
        } catch (ClassNotFoundException e) {
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

    private void fechar() {
        try {
            comando.close();
            con.close();
            System.out.println("Conexão Fechada");
        } catch (SQLException e) {
            imprimeErro("Erro ao fechar conexão", e.getMessage());
        }
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }
    
    
}
