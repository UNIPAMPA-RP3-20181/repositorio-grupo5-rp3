/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.List;
import javax.swing.JOptionPane;
import model.Usuario;

/**
 *
 * @author Leonardo
 * @email leopedroso45@gmail.com
 *
 */
public class UsuarioDAO implements DAO<Usuario> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    @Override
    public boolean insert(Usuario obj) {
        Usuario cand = obj;
        conectar();
        try {
            String sql = "INSERT INTO usuario(nomeUsuario, senhaUsuario, emailUsuario, tipoUsuario)"
                    + "VALUES ('" + cand.getNomeLogin() + "', '" + cand.getSenhaLogin() + "', '" + cand.getEmailCadastro() + "', '" + 1 + "');";
            comando.executeUpdate(sql);
            System.out.println("Inserido com sucesso!");
            fechar();
            return true;
        } catch (SQLException e) {
            if (e instanceof SQLIntegrityConstraintViolationException) {
                JOptionPane.showMessageDialog(null, "Este nome de usuário já foi cadastrado!");
                fechar();
                return false;

            } else {
                imprimeErro("Erro ao inserir Usuario", e.getMessage());
                fechar();
                return false;
            }
        } finally {
            fechar();
        }
    }

    @Override
    public void remove(int id) {

    }

    @Override
    public void update(Usuario user, int id) {
        conectar();
        try {
            //Connection conn = ConexaoBanco.abrir();
            String sql = "UPDATE usuario "
                    + "SET usuario.nomeUsuario = '" + user.getNomeLogin() + "', usuario.senhaUsuario = '" + user.getSenhaLogin() + "', usuario.emailUsuario = '" + user.getEmailCadastro() + "'  "
                    + "WHERE usuario.idUsuario = " + id + " ;";
            comando.executeUpdate(sql);
            //PreparedStatement pst = conn.prepareStatement(sql);
            //ResultSet rs = pst.executeQuery();
        } catch (SQLException e) {
            imprimeErro("Erro ao atualizar email!", e.getMessage());
            fechar();
        } finally {
            fechar();
        }
    }

    public Usuario select(int id) {
        Usuario user = new Usuario();;
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * from usuario, candidato WHERE "
                    + "candidato.idCandidato = '" + id + "' and "
                    + "candidato.Usuario_idUsuario = usuario.idUsuario");//<---
            ResultSet rs = pst.executeQuery();
            if (rs.next()) { 
                user.setNomeLogin(rs.getString("nomeUsuario"));
                user.setEmailCadastro(rs.getString("emailUsuario"));
                user.setSenhaLogin(rs.getString("senhaUsuario"));
  //              fechar();
                return user;
            }
        } catch (Exception e) {
            imprimeErro("Erro ao selecionar o Usuario", e.getMessage());
        } finally {

            return user;
        }
    }

    @Override
    public List selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void conectar() {
        try {
            con = ConFactory.conexao(URL, USUARIO, SENHA, ConFactory.MYSQL);
            comando = con.createStatement();
            System.out.println("Conectado!");
        } catch (ClassNotFoundException e) {
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

    private void fechar() {
        try {
            comando.close();
            con.close();
            System.out.println("Conexão Fechada");
        } catch (SQLException e) {
            imprimeErro("Erro ao fechar conexão", e.getMessage());
        }
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }

    @Override
    public Usuario select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
