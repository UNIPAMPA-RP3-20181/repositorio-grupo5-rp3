/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Solicitacao;

/**
 *
 * @author roliv
 */
public class SolicitacaoDAO implements DAO<Solicitacao> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    @Override
    public boolean insert(Solicitacao sol) {
        conectar();
        try {
            String sql = "INSERT INTO solicitacaoseguro(valorSolicitacaoSeguro, PredispCancerSeguro, "
                    + "PredispDiabeteSeguro, PredispDemenciaSeguro, PredispCoracaoSeguro, "
                    + "PredispCerebralSeguro, PredispPulmonarSeguro, PredispDegeneracaoSeguro, "
                    + "alcoolatra, fumante, DataSolicitacao, Candidato_idCandidato, StatusSolicitacaoCorretor, StatusSolicitacaoAvaliador, ItemProblemaSaude_idItemProblemaSaude)"
                    + "VALUES ('" + sol.getValor() + "', '" + sol.getPreCancer() + "', '" + sol.getPreDiabetes()
                    + "', '" + sol.getPreDemencia() + "', '" + sol.getPreCoracao() + "', '" + sol.getPreCerebral()
                    + "', '" + sol.getPrePulmonar() + "', '" + sol.getPreDegeneracao() + "', '" + sol.isAlcoolatra()
                    + "', '" + sol.isFumante() + "', '" + sol.getDataSolicitacao() + "', '" + sol.getIdCandidato() + "', '" + 3 + "', '" + 3 + "', "
                    + " (SELECT itemproblemasaude.idItemProblemaSaude FROM itemproblemasaude ORDER BY itemproblemasaude.idItemProblemaSaude DESC LIMIT 1))";

            comando.executeUpdate(sql);
            System.out.println("Inserido com sucesso!");
            fechar();
            return true;
        } catch (SQLException e) {
            imprimeErro("Erro ao inserir Solicitação", e.getMessage());
            return false;
        } finally {
            fechar();
        }
    }

    @Override
    public void remove(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Solicitacao obj, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Solicitacao select(int id) {
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * FROM solicitacaoseguro"
                    + " WHERE solicitacaoseguro.Candidato_idCandidato = '" + id + "'");//<---
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                Solicitacao sol = new Solicitacao();

                sol.setValor(rs.getDouble("valorSolicitacaoSeguro"));
                sol.setStatusCorretor(rs.getInt("StatusSolicitacaoCorretor"));
                sol.setStatusAvaliador(rs.getInt("StatusSolicitacaoAvaliador"));
                sol.setMotivoReprovacaoCorretor(rs.getString("MotivoReprovacaoCorretor"));
                sol.setMotivoReprovacaoAvaliador(rs.getString("MotivoReprovacaoAvaliador"));
                sol.setValorCorrigido(rs.getInt("ValorCorrigidoSeguro"));
                sol.setDataSolicitacao(rs.getString("DataSolicitacao"));

                return sol;
            }
        } catch (Exception e) {
            imprimeErro("Erro ao dar select na solicitao", e.getMessage());
        }
        return null;
    }

    public List<Solicitacao> selectAll(int stats) {
        List<Solicitacao> lista = new ArrayList<>();
        Solicitacao sol;
 
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * from solicitacaoseguro"
                    + " WHERE solicitacaoseguro.StatusSolicitacaoCorretor  = '" + stats + "'");//<---
            ResultSet rs = pst.executeQuery();
            sol = new Solicitacao();
            while (rs.next()) {

                sol.setValor(rs.getDouble("valorSolicitacaoSeguro"));
                sol.setPreCancer(rs.getInt("PredispCancerSeguro"));
                sol.setPreDiabetes(rs.getInt("PredispDiabeteSeguro"));
                sol.setPreDemencia(rs.getInt("PredispDemenciaSeguro"));
                sol.setPreCoracao(rs.getInt("PredispCoracaoSeguro"));
                sol.setPreCerebral(rs.getInt("PredispCerebralSeguro"));
                sol.setPrePulmonar(rs.getInt("PredispPulmonarSeguro"));
                sol.setPreDegeneracao(rs.getInt("PredispDegeneracaoSeguro"));
                sol.setAlcoolatra(rs.getBoolean("alcoolatra"));
                sol.setFumante(rs.getBoolean("fumante"));
                sol.setDataSolicitacao(rs.getString("DataSolicitacao"));
                sol.setIdCandidato(rs.getInt("Candidato_idCandidato"));
                lista.add(sol);
            }
            
            
        } catch (Exception e) {
            imprimeErro("Erro ao selecionar todos beneficiarios", e.getMessage());
        } finally {
            return lista;
        }
    }

    @Override
    public <T> List<T> selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void conectar() {
        try {
            con = ConFactory.conexao(URL, USUARIO, SENHA, ConFactory.MYSQL);
            comando = con.createStatement();
            System.out.println("Conectado!");
        } catch (ClassNotFoundException e) {
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

    private void fechar() {
        try {
            comando.close();
            con.close();
            System.out.println("Conexão Fechada");
        } catch (SQLException e) {
            imprimeErro("Erro ao fechar conexão", e.getMessage());
        }
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }

    @Override
    public Solicitacao select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
