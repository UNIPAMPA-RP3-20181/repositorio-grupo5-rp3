/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.swing.JOptionPane;
import model.RelatoMorte;

/**
 *
 * @author LeonardoSevero
 */
public class RelatoMorteDAO implements DAO<RelatoMorte> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    @Override
    public boolean insert(RelatoMorte obj) {
        RelatoMorte rm = obj;
        conectar();
        try {
            String sql = "INSERT INTO relatomorte(nomeSegurado, numeroApolice, causaMorte, nomeContato, parentesco, telefone, email, pdf)"
                    + "VALUES ('" + rm.getNomeSegurado() + "', '" + rm.getNumApolice() + "', '" + rm.getCausaMorte()
                    + "', '" + rm.getNomeContato() + "', '" + rm.getParentesco() + "', '" + rm.getTelefone()
                    + "', '" + rm.getEmail() + "', '" + rm.getCertidaoObito() + "')";
            comando.executeUpdate(sql);
            System.out.println("Relato Inserido com sucesso!!!");
            fechar();
            return true;
        } catch (SQLException e) {
            imprimeErro("Erro ao inserir Relato", e.getMessage());
            fechar();
            return false;
        } finally {
            fechar();
        }
    }

    @Override
    public void remove(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(RelatoMorte obj, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RelatoMorte select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T> List<T> selectAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void conectar() {
        try {
            con = ConFactory.conexao(URL, USUARIO, SENHA, ConFactory.MYSQL);
            comando = con.createStatement();
            System.out.println("Conectado!");
        } catch (ClassNotFoundException e) {
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

    private void fechar() {
        try {
            comando.close();
            con.close();
            System.out.println("Conexão Fechada");
        } catch (SQLException e) {
            imprimeErro("Erro ao fechar conexão", e.getMessage());
        }
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }

}
