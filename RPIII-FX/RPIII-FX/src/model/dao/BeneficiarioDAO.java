/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Beneficiario;

/**
 *
 * @author Leonardo email: leopedroso45@gmail.com
 *
 */
public class BeneficiarioDAO implements DAO<Beneficiario> {

    private final String URL = "jdbc:mysql://localhost:3306/corretora", USUARIO = "root", SENHA = "";
    private Connection con;
    private Statement comando;

    public BeneficiarioDAO() {

    }

 
    @Override
    public boolean insert(Beneficiario ben) {
        conectar();
        try {
            String sql = "INSERT INTO beneficiario(nomeBeneficiario, emailBeneficiario, parentescoBeneficiario, contatoBeneficiario, Candidato_idCandidato)"
                    + "VALUES ('" + ben.getNome() + "', '" + ben.getEmail() + "', '" + ben.getParentesco() + "', '" + ben.getContato() + "', '" + ben.getIdCandidato() + "');";
            comando.executeUpdate(sql);
            System.out.println("Beneficiario Inserido com sucesso!");
            fechar();
            return true;
        } catch (SQLException e) {
            imprimeErro("Erro ao inserir Beneficiario", e.getMessage());
            fechar();
            return false;
        } finally {
            fechar();
        }
    }

    @Override
    public void remove(int id) {
        conectar();
        try {
            String sql = "DELETE from beneficiario WHERE beneficiario.idBeneficiario =  " + id + " ;";
            comando.executeUpdate(sql);
        } catch (Exception e) {
            imprimeErro("Erro ao remover Beneficiario", e.getMessage());
        } finally {
            fechar();
        }

    }

    /**
     * @falta implementar esse
     * @param obj
     */
    @Override
    public void update(Beneficiario obj, int id) {
        Beneficiario ben = obj;
    }

    @Override
    public Beneficiario select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    public List<Beneficiario> selectAll(int idCand) {
        List<Beneficiario> lista = new ArrayList<>();
        Beneficiario bene;
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT * from beneficiario,"
                    + " candidato WHERE beneficiario.Candidato_idCandidato = '" + idCand + "'");//<---
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                bene = new Beneficiario();
                bene.setIdBene(rs.getInt("idBeneficiario"));
                bene.setNome(rs.getString("nomeBeneficiario"));
                bene.setEmail(rs.getString("emailBeneficiario"));
                bene.setParentesco(rs.getString("parentescoBeneficiario"));
                bene.setContato(rs.getString("contatoBeneficiario"));
                lista.add(bene);
            }
        } catch (Exception e) {
            imprimeErro("Erro ao selecionar todos beneficiarios", e.getMessage());
        } finally {
            return lista;
        }
    }

    @Override
    public List<Beneficiario> selectAll() {
        List<Beneficiario> listaBeneficiarios = new <Beneficiario>ArrayList();
        try {
            Connection conn = ConexaoBanco.abrir();
            PreparedStatement pst = conn.prepareStatement("SELECT beneficiario.* from beneficiario, candidato where Candidato_idCandidato = beneficiario.Candidato_idCandidato");
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Beneficiario bene = new Beneficiario();
                bene.setNome(rs.getString("nomeBeneficiario"));
                bene.setEmail(rs.getString("emailBeneficiario"));
                bene.setParentesco(rs.getString("parentescoBeneficiario"));
                bene.setContato(rs.getString("contatoBeneficiario"));
                listaBeneficiarios.add(bene);
            }
        } catch (Exception e) {
            imprimeErro("Erro ao pegar todos Beneficiarios", e.getMessage());
        } finally {
            return listaBeneficiarios;
        }
    }

    private void conectar() {
        try {
            con = ConFactory.conexao(URL, USUARIO, SENHA, ConFactory.MYSQL);
            comando = con.createStatement();
            System.out.println("Conectado!");
        } catch (ClassNotFoundException e) {
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

    private void fechar() {
        try {
            comando.close();
            con.close();
            System.out.println("Conexão Fechada");
        } catch (SQLException e) {
            imprimeErro("Erro ao fechar conexão", e.getMessage());
        }
    }

    private void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }

}
