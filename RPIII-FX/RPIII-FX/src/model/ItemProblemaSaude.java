/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author roliv
 */
public class ItemProblemaSaude {
    private String resultado;
    private String observacao;
    private List<ProblemaSaude> problemas = new ArrayList();

    public ItemProblemaSaude(String resultado, String observacao, List<ProblemaSaude> problemas) {
        this.resultado = resultado;
        this.observacao = observacao;
        this.problemas = problemas;
    }
    
    public ItemProblemaSaude(){}

    public List<ProblemaSaude> getProblemas() {
        return problemas;
    }

    public void setProblemas(List<ProblemaSaude> problemas) {
        this.problemas = problemas;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
 
}
