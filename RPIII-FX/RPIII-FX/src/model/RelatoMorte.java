/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;

/**
 *
 * @author roliv
 */
public class RelatoMorte {

    private String nomeSegurado;
    private int numApolice;
    private String causaMorte;
    private String nomeContato;
    private String parentesco;
    private String telefone;
    private String email;
    private File certidaoObito;

    public RelatoMorte(String nomeSegurado, int numApolice, String causaMorte, String nomeContato, String parentesco, String telefone, String email, File certidaoObito) {
        this.nomeSegurado = nomeSegurado;
        this.numApolice = numApolice;
        this.causaMorte = causaMorte;
        this.nomeContato = nomeContato;
        this.parentesco = parentesco;
        this.telefone = telefone;
        this.email = email;
        this.certidaoObito = certidaoObito;
    }

    public String getNomeSegurado() {
        return nomeSegurado;
    }

    public void setNomeSegurado(String nomeSegurado) {
        this.nomeSegurado = nomeSegurado;
    }

    public int getNumApolice() {
        return numApolice;
    }

    public void setNumApolice(int numApolice) {
        this.numApolice = numApolice;
    }

    public String getCausaMorte() {
        return causaMorte;
    }

    public void setCausaMorte(String causaMorte) {
        this.causaMorte = causaMorte;
    }

    public String getNomeContato() {
        return nomeContato;
    }

    public void setNomeContato(String nomeContato) {
        this.nomeContato = nomeContato;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public File getCertidaoObito() {
        return certidaoObito;
    }

    public void setCertidaoObito(File certidaoObito) {
        this.certidaoObito = certidaoObito;
    }
    
    
}
