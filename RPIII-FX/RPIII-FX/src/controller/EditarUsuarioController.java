/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import model.Usuario;
import model.dao.DAOMestra;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class EditarUsuarioController extends ControllerAbs implements Initializable {

    public void setUsuarioInput(String usuarioInput) {
        this.usuarioInput.setText(usuarioInput); 
    }

    public void setSenhaInput(String senhaInput) {
        this.senhaInput.setText(senhaInput);
    }

    public void setConfirmInput(String confirmInput) {
        this.confirmInput.setText(confirmInput);
    }

    public void setEmailInput(String emailInput) {
        this.emailInput.setText(emailInput);
    }
    
    @FXML
    private TextField usuarioInput;

    @FXML
    private PasswordField senhaInput;

    @FXML
    private PasswordField confirmInput;

    @FXML
    private TextField emailInput;

    @FXML
    private Button buttonVoltar;

    @FXML
    private Button buttonConfirm;

    @FXML
    void confirmarEditar(ActionEvent event) {
        if (Validacao.validarDescricao(usuarioInput.getText()) != true
                || Validacao.validarDescricao(senhaInput.getText()) != true
                || Validacao.validarDescricao(confirmInput.getText()) != true
                || Validacao.validarDescricao(emailInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        } else if (Validacao.validarChar(usuarioInput.getText()) != true
                || Validacao.validarChar(senhaInput.getText()) != true
                || Validacao.validarChar(confirmInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite apenas valores válidos!");
        } else if (Validacao.validarSenha(senhaInput.getText(), confirmInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "A senha e confirmação de senha são diferentes!");
        } else if (Validacao.validarTamanhoSenha(confirmInput.getText())) {
            JOptionPane.showMessageDialog(null, "A senha deve ter no mínimo 6 caracteres!");
        } else if (Validacao.validarEmail(emailInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite um e-mail válido!");
            emailInput.setText("");
        } else {
            Usuario user = new Usuario();
            user.setNomeLogin(usuarioInput.getText());
            user.setSenhaLogin(senhaInput.getText());
            user.setEmailCadastro(emailInput.getText());
            DAOMestra dao = new DAOMestra();
            if (dao.atualizarUsuario(user, idCand)) {
                JOptionPane.showMessageDialog(null, "Dados de Usuário atualizados com sucesso!");
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(super.desempilharAnte());
                app_stage.show();
            }

        }
    }

    @FXML
    void voltarCandidato(ActionEvent event) {
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(super.desempilharAnte());
        app_stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb
    ) {
        // TODO
    }

}
