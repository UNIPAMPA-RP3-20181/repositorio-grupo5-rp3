/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class CadastroCandidato3Controller extends ControllerAbs implements Initializable {

    private String nome;
    private String dataNascimento;
    private String cpfInput;
    private String genero;
    private String estadoCivil;
    private String ocupacao;
    private String ruaInput;
    private String bairroInput;
    private String cidadeInput;
    private String telInput;
    private String emailInput;
    private String numInput;
    private String cepInput;
    private String estInput;
    private String celInput;
    private String complementoInput;
    private Image image;

    public void setComplementoInput(String complementoInput) {
        this.complementoInput = complementoInput;
        this.compleLabel.setText(complementoInput);
    }

    public void setImage(Image image) {
        this.image = image;
        this.imgUser3.setImage(image);
    }

    public void setNome(String nome) {
        this.nome = nome;
        this.nomeLabel.setText(nome);
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
        this.dataLabel.setText(dataNascimento);
    }

    public void setCpfInput(String cpfInput) {
        this.cpfInput = cpfInput;
        this.cpfLabel.setText(cpfInput);
    }

    public void setGenero(String genero) {
        this.genero = genero;
        this.genLabel.setText(genero);
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
        this.civilLabel.setText(estadoCivil);
    }

    public void setOcupacao(String ocupacao) {
        this.ocupacao = ocupacao;
        this.opeLabel.setText(ocupacao);
    }

    public void setRuaInput(String rua) {
        this.ruaInput = rua;
        this.ruaLabel.setText(rua);
    }

    public void setBairroInput(String bairro) {
        this.bairroInput = bairro;
        this.bairroLabel.setText(bairro);
    }

    public void setCidadeInput(String cidade) {
        this.cidadeInput = cidade;
        this.cidLabel.setText(cidade);
    }

    public void setTelInput(String telefone) {
        this.telInput = telefone;
        this.telLabel.setText(telefone);
    }

    public void setEmailInput(String email) {
        this.emailInput = email;
        this.emailLabel.setText(email);
    }

    public void setNumInput(String numero) {
        this.numInput = numero;
        this.numLabel.setText(numero);
    }

    public void setCepInput(String cep) {
        this.cepInput = cep;
        this.cepLabel.setText(cep);
    }

    public void setEstInput(String estado) {
        this.estInput = estado;
        this.estLabel.setText(estado);
    }

    public void setCelInput(String celular) {
        this.celInput = celular;
        this.celLabel.setText(celular);
    }

    @FXML
    private Label compleLabel;

    @FXML
    private ImageView imgUser3;

    @FXML
    private Label nomeLabel;

    @FXML
    private Label dataLabel;

    @FXML
    private Label cpfLabel;

    @FXML
    private Label genLabel;

    @FXML
    private Label civilLabel;

    @FXML
    private Label opeLabel;

    @FXML
    private Label ruaLabel;

    @FXML
    private Label bairroLabel;

    @FXML
    private Label cidLabel;

    @FXML
    private Label telLabel;

    @FXML
    private Label emailLabel;

    @FXML
    private Label numLabel;

    @FXML
    private Label cepLabel;

    @FXML
    private Label estLabel;

    @FXML
    private Label celLabel;

    @FXML
    private Button buttonVoltar;

    @FXML
    private Button buttonPross;

    @FXML
    void prossCadastro(ActionEvent event) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroFinal.fxml"));
        Parent menu_parent = loader.load();
        CadastroFinalController controller = loader.<CadastroFinalController>getController();

        controller.setImagePerfil(image);
        controller.setNome(nome);
        controller.setDataNascimento(dataNascimento);
        controller.setCpfInput(cpfInput);
        controller.setGenero(genero);
        controller.setEstadoCivil(estadoCivil);
        controller.setOcupacao(ocupacao);
        controller.setRuaInput(ruaInput);
        controller.setBairroInput(bairroInput);
        controller.setCidadeInput(cidadeInput);
        controller.setTelInput(telInput);
        controller.setEmailInput(emailInput);
        controller.setNumInput(numInput);
        controller.setCepInput(cepInput);
        controller.setEstInput(estInput);
        controller.setCelInput(celInput);
        controller.setComplementoInput(complementoInput);

        super.empilharAnte(((Node) event.getSource()).getScene());

        if (proxVazia()) {
            Scene menu_scene = new Scene(menu_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(menu_scene);
            app_stage.show();
        } else {
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(super.desempilharProx());
            app_stage.show();
        }
    }

    @FXML
    void voltarAnterior(ActionEvent event) throws IOException {
        empilharProx(((Node) event.getSource()).getScene());
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(super.desempilharAnte());
        app_stage.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}
