/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class MenuCorretorController extends ControllerAbs implements Initializable {

    public void setNomeCorretor(String nomeCorretor) {
        this.nomeCorretor.setText(nomeCorretor);
    }

    public void setEmailLabel(String emailLabel) {
        this.emailLabel.setText(emailLabel);
    }
    
    @FXML
    private Label nomeCorretor;

    @FXML
    private Label emailLabel;

    @FXML
    private Button buttonPendentes;

    @FXML
    private Button buttonExit;

    @FXML
    void avaliarPendentes(ActionEvent event) throws IOException {
        super.empilharAnte(((Node) event.getSource()).getScene());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AvaliacoesPendentesCorretor.fxml"));
        Parent menu_parent = loader.load();
        
        if (proxVazia()) {
            Scene menu_scene = new Scene(menu_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(menu_scene);
            app_stage.show();
        } else {
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(super.desempilharProx());
            app_stage.show();
        }
    }

    @FXML
    void editarPerfil(ActionEvent event) throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/EditarPerfilCorretor.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
    }

    @FXML
    void sairMenu(ActionEvent event) throws IOException {
        esvaziarPilhas();
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/FXMLHomePage.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
