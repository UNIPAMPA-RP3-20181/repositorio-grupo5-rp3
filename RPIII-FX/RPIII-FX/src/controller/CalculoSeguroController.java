/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import model.Solicitacao;
import model.dao.DAOMestra;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class CalculoSeguroController extends ControllerAbs implements Initializable {

    private double total;
    private Solicitacao solicitacao;

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
        this.labelValor.setText(String.valueOf(this.total));
    }

    public Solicitacao getSolicitacao() {
        return solicitacao;
    }

    public void setSolicitacao(Solicitacao solicitacao) {
        this.solicitacao = solicitacao;
    }

    @FXML
    private Label labelValor;

    @FXML
    private Button buttonAceitar;

    @FXML
    private Button buttonRecusar;

    @FXML
    private Button buttonVoltarSol;

    @FXML
    void voltarSolicitacao(ActionEvent event) throws IOException {
        empilharProx(((Node) event.getSource()).getScene());
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(super.desempilharAnte());
        app_stage.show();

    }

    @FXML
    void aceitarValor(ActionEvent event) {
        DAOMestra mestra = new DAOMestra();
        if (mestra.RegistrarSolicitacao(solicitacao)) {
            JOptionPane.showMessageDialog(null, "Solicitação feita com sucesso!");
            super.desempilharAnte();
            super.desempilharAnte();
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(super.desempilharAnte());
            app_stage.show();
        }
    }
    
    @FXML
    void recusarValor(ActionEvent event) {

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}
