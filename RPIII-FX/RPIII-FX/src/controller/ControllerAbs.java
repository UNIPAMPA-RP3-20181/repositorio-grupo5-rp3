/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import javafx.scene.Scene;

/**
 *
 * @author roliv
 */
public abstract class ControllerAbs {

    static ArrayList<Scene> anterior = new ArrayList<>();
    static ArrayList<Scene> proximo = new ArrayList<>();
    static int idCand;
    private int posicaoAnte;
    private int posicaoProx;

    public static int getIdCand() {
        return idCand;
    }

    public static void setIdCand(int idCand) {
        ControllerAbs.idCand = idCand;
    }
    
    
    public int getPosicaoAnte() {
        return posicaoAnte;
    }

    public void setPosicaoAnte(int posicaoAnte) {
        this.posicaoAnte = posicaoAnte;
    }

    public int getPosicaoProx() {
        return posicaoProx;
    }

    public void setPosicaoProx(int posicaoProx) {
        this.posicaoProx = posicaoProx;
    }
    
    

    public ControllerAbs() {
        this.posicaoAnte = -1;
        this.posicaoProx = -1;
    }

    public void empilharProx(Scene cena) {
            proximo.add(cena);
    }

    public void empilharAnte(Scene cena) {
        anterior.add(cena);
        System.out.println("empilhou!");
    }

    public boolean proxVazia() {
        if (proximo.isEmpty()) {
            return true;
        }
        return false;
    }

    public boolean anteVazia() {
        if (anterior.isEmpty()) {
            return true;
        }
        return false;
    }

    public Scene desempilharProx() {
        int i = 0;
        if (proxVazia()) {
            return null;  
        }
        i = proximo.size() - 1;
        Scene cena = proximo.get(i);
        proximo.remove(i);
        return cena;
    }

    public Scene desempilharAnte() {   
        int i = 0;
        if (anteVazia()) {
            return null;
        }
        i = anterior.size() - 1;
        Scene cena = anterior.get(i);
        anterior.remove(i);
        return cena;
    }
    
    public void esvaziarPilhas(){
        anterior.clear();
        proximo.clear();
    }

}
