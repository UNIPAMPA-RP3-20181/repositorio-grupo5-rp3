/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class CadastroCandidato2Controller extends ControllerAbs implements Initializable {

    private String nome;
    private String dataNascimento;
    private String cpfInput;
    private String genero;
    private String estadoCivil;
    private String ocupacao;
    private Image image;

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public void setCpfInput(String cpfInput) {
        this.cpfInput = cpfInput;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public void setOcupacao(String ocupacao) {
        this.ocupacao = ocupacao;
    }

    public void setImage(Image image) {
        this.image = image;
        imgUser2.setImage(image);
    }

    @FXML
    private TextField complementoInput;

    @FXML
    private ImageView imgUser2;

    @FXML
    private TextField ruaInput;

    @FXML
    private TextField bairroInput;

    @FXML
    private TextField cidadeInput;

    @FXML
    private TextField telInput;

    @FXML
    private TextField emailInput;

    @FXML
    private TextField numInput;

    @FXML
    private TextField cepInput;

    @FXML
    private TextField estInput;

    @FXML
    private TextField celInput;

    @FXML
    private Button buttonVoltar;

    @FXML
    private Button buttonPross;

    @FXML
    void prossCadastro2(ActionEvent event) throws IOException {

        if (Validacao.validarDescricao(ruaInput.getText()) != true || Validacao.validarDescricao(numInput.getText()) != true
                || Validacao.validarDescricao(bairroInput.getText()) != true || Validacao.validarDescricao(cidadeInput.getText()) != true
                || Validacao.validarDescricao(estInput.getText()) != true || Validacao.validarDescricao(cepInput.getText()) != true
                || Validacao.validarDescricao(telInput.getText()) != true || Validacao.validarDescricao(celInput.getText()) != true
                || Validacao.validarDescricao(emailInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        } else if (Validacao.validarNome(cidadeInput.getText()) != true || Validacao.validarNome(estInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite apenas valores válidos!");
        } else if (Validacao.validarEmail(emailInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite um e-mail válido!");
            emailInput.setText("");
        } else {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroCandidato3.fxml"));
            Parent menu_parent = loader.load();
            CadastroCandidato3Controller controller = loader.<CadastroCandidato3Controller>getController();

            controller.setNome(nome);
            controller.setDataNascimento(dataNascimento);
            controller.setCpfInput(cpfInput);
            controller.setGenero(genero);
            controller.setEstadoCivil(estadoCivil);
            controller.setOcupacao(ocupacao);
            controller.setBairroInput(genero);
            controller.setRuaInput(ruaInput.getText());
            controller.setBairroInput(bairroInput.getText());
            controller.setCidadeInput(cidadeInput.getText());
            controller.setTelInput(telInput.getText());
            controller.setEmailInput(emailInput.getText());
            controller.setNumInput(numInput.getText());
            controller.setCepInput(cepInput.getText());
            controller.setEstInput(estInput.getText());
            controller.setCelInput(celInput.getText());
            controller.setComplementoInput(complementoInput.getText());
            controller.setImage(image);

            super.empilharAnte(((Node) event.getSource()).getScene());

            if (proxVazia()) {
                Scene menu_scene = new Scene(menu_parent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(menu_scene);
                app_stage.show();
            } else {
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(super.desempilharProx());
                app_stage.show();
            }

        }
    }

    @FXML
    void voltarCadastro(ActionEvent event) throws IOException {
        empilharProx(((Node) event.getSource()).getScene());
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(super.desempilharAnte());
        app_stage.show();

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb
    ) {
        MaskField.cepField(cepInput);
        MaskField.foneField(telInput);
        MaskField.foneField(celInput);
        MaskField.numericField(numInput);

    }

}
