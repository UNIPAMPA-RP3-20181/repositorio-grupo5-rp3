/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import model.dao.BeneficiarioDAO;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class SolicitacoesPendentesController extends ControllerAbs implements Initializable {

    private int idade;
    private int genero;

    public void setGenero(String genero) {
        if (genero.equals("Feminino")) {
            this.genero = 1;
        } else {
            this.genero = 0;
        }
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    @FXML
    private Button buttonVoltar;

    @FXML
    private Button buttonSolicitar;

    @FXML
    private Button buttonAcom;

    @FXML
    void acompanharSol(ActionEvent event) throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/AcompanharSolicitacao.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();

    }

    @FXML
    void solicitarSeg(ActionEvent event) throws IOException {
        BeneficiarioDAO dao = new BeneficiarioDAO();
        if (dao.selectAll(idCand).size() > 0) {
            super.empilharAnte(((Node) event.getSource()).getScene());
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Solicitacao.fxml"));
            Parent menu_parent = loader.load();
            SolicitacaoController controller = loader.<SolicitacaoController>getController();
            controller.setIdade(idade);
            controller.setGen(genero);
            if (proxVazia()) {
                Scene menu_scene = new Scene(menu_parent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(menu_scene);
                app_stage.show();
            } else {
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(super.desempilharProx());
                app_stage.show();
            }
        }else{
             JOptionPane.showMessageDialog(null, "Você precisa cadastrar pelo menos um beneficiário antes de fazer a solicitação!");
        }

    }

    @FXML
    void voltarMenu(ActionEvent event) throws IOException {
        empilharProx(((Node) event.getSource()).getScene());
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(super.desempilharAnte());
        app_stage.show();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

}
