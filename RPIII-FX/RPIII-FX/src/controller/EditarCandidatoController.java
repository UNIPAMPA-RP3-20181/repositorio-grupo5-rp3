/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.jfoenix.controls.JFXDatePicker;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import model.Beneficiario;
import model.Candidato;
import model.Endereco;
import model.Usuario;
import model.dao.DAOMestra;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class EditarCandidatoController extends ControllerAbs implements Initializable {

    private String genero;
    private String date;
    private ObservableList<String> estadoCivil = FXCollections.observableArrayList("Solteiro(a)", "Casado(a)",
            "Viúvo(a)", "Divorciado(a)", "Separado(a)");
    private List<Beneficiario> listaBeneficiario;
    private ToggleGroup group = new ToggleGroup();

    public void setGenero(String genero) {
        if (genero.equals("Masculino")) {
            genMasc.setSelected(true);
        } else if (genero.equals("Feminino")) {
            genFem.setSelected(true);
        } else {
            genOut.setSelected(true);
        }
    }

    public void setImgUser(ImageView imgUser) {
        this.imgUser = imgUser;
    }

    public void setNomeInput(String nome) {
        this.nomeInput.setText(nome);
    }

    public void setDataImput(String dataNas) {
        String[] data = dataNas.split("/");
        LocalDate birthDate = LocalDate.of(Integer.parseInt(data[2]), Integer.parseInt(data[1]), Integer.parseInt(data[0]));
        this.dataImput.setValue(birthDate);
    }

    public void setCivilCombo(String estadoCivil) {
        this.civilCombo.setValue(estadoCivil);
    }

    public void setOcupInput(String ocupacao) {
        this.ocupInput.setText(ocupacao);
    }

    public void setRuaInput(String rua) {
        this.ruaInput.setText(rua);
    }

    public void setBairroInput(String bairro) {
        this.bairroInput.setText(bairro);
    }

    public void setCidInput(String cidade) {
        this.cidInput.setText(cidade);
    }

    public void setTelInput(String telefone) {
        this.telInput.setText(telefone);
    }

    public void setNumInput(String numero) {
        this.numInput.setText(numero);
    }

    public void setCepInput(String cep) {
        this.cepInput.setText(cep);
    }

    public void setEstInput(String estado) {
        this.estInput.setText(estado);
    }

    public void setCelInput(String celular) {
        this.celInput.setText(celular);
    }

    @FXML
    private Button escolherFoto;

    @FXML
    private ImageView imgUser;

    @FXML
    private TextField nomeInput;

    @FXML
    private JFXDatePicker dataImput;

    @FXML
    private RadioButton genMasc;

    @FXML
    private RadioButton genFem;

    @FXML
    private RadioButton genOut;

    @FXML
    private ComboBox civilCombo;

    @FXML
    private TextField ocupInput;

    @FXML
    private TextField ruaInput;

    @FXML
    private TextField bairroInput;

    @FXML
    private TextField cidInput;

    @FXML
    private TextField telInput;

    @FXML
    private TextField numInput;

    @FXML
    private TextField complementoInput;

    @FXML
    private TextField cepInput;

    @FXML
    private TextField estInput;

    @FXML
    private TextField celInput;

    @FXML
    private Button buttonVoltar;

    @FXML
    private Button buttonConfirm;

    @FXML
    void confirmarEditar(ActionEvent event) throws IOException {
        if (Validacao.validarDescricao(nomeInput.getText()) != true || dataImput.getValue() == null
                || civilCombo.getValue().equals("Selecione") || Validacao.validarDescricao(ocupInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        } else if (Validacao.validarNome(nomeInput.getText()) != true || dataImput.getValue() == null
                || civilCombo.getValue().equals("Selecione") || Validacao.validarNome(ocupInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite apenas valores válidos!");
        } else if (Validacao.validarDescricao(ruaInput.getText()) != true || Validacao.validarDescricao(numInput.getText()) != true
                || Validacao.validarDescricao(bairroInput.getText()) != true || Validacao.validarDescricao(cidInput.getText()) != true
                || Validacao.validarDescricao(estInput.getText()) != true || Validacao.validarDescricao(cepInput.getText()) != true
                || Validacao.validarDescricao(telInput.getText()) != true || Validacao.validarDescricao(celInput.getText()) != true){
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        } else if (Validacao.validarNome(cidInput.getText()) != true || Validacao.validarNome(estInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite apenas valores válidos!");
        } else if (dataImput.getValue().compareTo(calculateMinYerar()) >= 1) {
            JOptionPane.showMessageDialog(null, "É preciso ser maior de idade para se cadastrar!");
        } else {
            this.date = dataImput.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            if (genMasc != null) {
                this.genero = "Masculino";
            } else if (genFem != null) {
                this.genero = "Feminino";
            } else {
                this.genero = "Outro";
            }
            Candidato candidato = new Candidato(new Usuario(" ", " ", " "),
                    listaBeneficiario, ocupInput.getText(), null, nomeInput.getText(), "", telInput.getText(), celInput.getText(), genero, String.valueOf(this.civilCombo.getValue()),
                    date, new Endereco(ruaInput.getText(), numInput.getText(), complementoInput.getText(), cepInput.getText(), bairroInput.getText(), cidInput.getText(), estInput.getText()));
            DAOMestra mestra = new DAOMestra();

            if (mestra.atualizarCandidato(candidato, getIdCand())) {
                JOptionPane.showMessageDialog(null, "Dados Atualizados com sucesso!");
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(super.desempilharAnte());
                app_stage.show();
            } else {
                JOptionPane.showMessageDialog(null, "Não foi possivel atualizados com sucesso!");
            }
        }
    }

    @FXML
    void escolherFoto(ActionEvent event) {

    }

    @FXML
    void voltarCandidato(ActionEvent event) throws IOException {
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(super.desempilharAnte());
        app_stage.show();
    }

    public LocalDate calculateMinYerar() {
        LocalDate currentDate = LocalDate.now();
        currentDate = currentDate.minusYears(18).minusDays(1);

        return currentDate;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        civilCombo.setItems(estadoCivil);
        MaskField.cepField(cepInput);
        MaskField.foneField(telInput);
        MaskField.foneField(celInput);
        MaskField.numericField(numInput);
        genFem.setToggleGroup(group);
        genMasc.setToggleGroup(group);
        genOut.setToggleGroup(group);

    }

}
