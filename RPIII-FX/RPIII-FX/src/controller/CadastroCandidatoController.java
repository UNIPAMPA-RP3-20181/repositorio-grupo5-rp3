/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import com.jfoenix.controls.JFXDatePicker;
import java.time.LocalDate;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class CadastroCandidatoController extends ControllerAbs implements Initializable {

    private String date;
    private String genero;
    private Image image;
    private ToggleGroup group = new ToggleGroup();
    private ObservableList<String> estadoCivil = FXCollections.observableArrayList("Solteiro(a)", "Casado(a)",
            "Viúvo(a)", "Divorciado(a)", "Separado(a)");

    @FXML
    private Button escolherFoto;

    @FXML
    private ImageView imgUser;

    @FXML
    private TextField nomeInput;

    @FXML
    private JFXDatePicker dataImput;

    @FXML
    private TextField cpfInput;

    @FXML
    private RadioButton genMasc;

    @FXML
    private RadioButton genFem;

    @FXML
    private RadioButton genOut;

    @FXML
    private ComboBox civilCombo;

    @FXML
    private TextField ocupInput;

    @FXML
    private Button buttonSair;

    @FXML
    private Button buttonPross;

    @FXML
    void escolherFoto(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
        File file = fileChooser.showOpenDialog(null);

        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            Image imageChooser = SwingFXUtils.toFXImage(bufferedImage, null);
            this.image = imageChooser;
            imgUser.setImage(image);

        } catch (IOException ex) {
            Logger.getLogger(CadastroCandidatoController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    void prossCadastro(ActionEvent event) throws IOException {
        if (Validacao.validarDescricao(nomeInput.getText()) != true || dataImput.getValue() == null
                || civilCombo.getValue().equals("Selecione") || Validacao.validarDescricao(ocupInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        } else if (Validacao.validarNome(nomeInput.getText()) != true || dataImput.getValue() == null
                || civilCombo.getValue().equals("Selecione") || Validacao.validarNome(ocupInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite apenas valores válidos!");
        } else if (Validacao.isCPF(cpfInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite um CPF válido!");
            cpfInput.setText("");
        } else if (dataImput.getValue().compareTo(calculateMinYerar()) >= 1) {
            JOptionPane.showMessageDialog(null, "É preciso ser maior de idade para se cadastrar!");
        } else {

            if (genMasc != null) {
                this.genero = "Masculino";
            } else if (genFem != null) {
                this.genero = "Feminino";
            } else {
                this.genero = "Outro";
            }

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CadastroCandidato2.fxml"));
            Parent menu_parent = loader.load();

            CadastroCandidato2Controller controller = loader.<CadastroCandidato2Controller>getController();
            controller.setDataNascimento(date);
            controller.setEstadoCivil(String.valueOf(this.civilCombo.getValue()));
            controller.setCpfInput(cpfInput.getText());
            controller.setOcupacao(ocupInput.getText());
            controller.setImage(image);
            controller.setNome(nomeInput.getText());
            controller.setGenero(genero);
            this.date = dataImput.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            controller.setDataNascimento(date);

            super.empilharAnte(((Node) event.getSource()).getScene());

            if (proxVazia()) {
                Scene menu_scene = new Scene(menu_parent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(menu_scene);
                app_stage.show();
            } else {
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(super.desempilharProx());
                app_stage.show();
            }

        }
//        else {
//            
//            
//            
//            controller.setEstadoCivil(String.valueOf(this.civilCombo.getValue()));
//            controller.setCpfInput(cpfInput.getText());
//            controller.setOcupacao(ocupInput.getText());
//            controller.setImage(image);
//
//            if (genMasc != null) {
//                this.genero = "Masculino";
//            } else if (genFem != null) {
//                this.genero = "Feminino";
//            } else {
//                this.genero = "Outro";
//            }
//            
//
//            Scene sceneCadastroCandidato2Controller = ((Node) event.getSource()).getScene();
//            controller.setSceneCadastroCandidato2(sceneCadastroCandidato2Controller);
//            Scene menu_scene = new Scene(menu_parent);
//            
//            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
//            app_stage.hide();
//            app_stage.setScene(sceneCadastroController);
//            app_stage.show();
//        }
    }

    @FXML
    void sairCadastro(ActionEvent event) throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/FXMLHomePage.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
    }

    public boolean validarCampo(String titulo) {
        if (titulo == null || " ".equals(titulo) || "".equals(titulo)) {
            return false;
        } else {
            Pattern pattern = Pattern.compile("[0-9]");
            Matcher matcher = pattern.matcher(titulo);
            if (matcher.find()) {
                return false;
            }
        }
        return true;
    }

    public LocalDate calculateMinYerar() {
        LocalDate currentDate = LocalDate.now();
        currentDate = currentDate.minusYears(18).minusDays(1);

        return currentDate;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        civilCombo.setValue("Selecione");
        civilCombo.setItems(estadoCivil);
        MaskField.cpfField(cpfInput);
        genFem.setToggleGroup(group);
        genMasc.setToggleGroup(group);
        genOut.setToggleGroup(group);

    }

}
