/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import model.Beneficiario;
import model.dao.BeneficiarioDAO;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class GerenciarBeneficiariosController extends ControllerAbs implements Initializable {

    int idCandidato;
    final ObservableList<Beneficiario> data = FXCollections.observableArrayList();

    public void setIdCandidato(int idCandidato) {
        this.idCandidato = idCandidato;
    }

    @FXML
    private Button buttonVoltarPendente;

    @FXML
    private TableView<Beneficiario> tableBene;

    @FXML
    private TableColumn<?, ?> colNome;

    @FXML
    private TableColumn<?, ?> colParent;

    @FXML
    private TableColumn<?, ?> colEmail;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonRemove;

    @FXML
    private Button buttonEdit;

    @FXML
    void adicionarBeneficiario(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AdicionarBeneficiario.fxml"));
        Parent menu_parent = loader.load();
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        //    app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();

    }

    @FXML
    void editarBeneficiario(ActionEvent event) {

    }

    @FXML
    void removerBeneficiario(ActionEvent event) {
        int index = tableBene.getSelectionModel().getSelectedIndex();
        Beneficiario bene = new Beneficiario();
        bene.setIdBene(tableBene.getItems().get(index).getIdBene());
        if (tableBene.getItems().removeAll(tableBene.getSelectionModel().getSelectedItems())) {
            BeneficiarioDAO dao = new BeneficiarioDAO();
            dao.remove(bene.getIdBene());
            JOptionPane.showMessageDialog(null, "Beneficiário removido com sucesso!");
        } else {
            JOptionPane.showMessageDialog(null, "Não foi possível remover o beneficiário!");
        }

    }

    @FXML
    void voltarPendente(ActionEvent event) throws IOException {
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(super.desempilharAnte());
        app_stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tableBene.setPlaceholder(new Label("Nenhum problema de saúde foi adicionado"));
        tableBene.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("nome"));
        tableBene.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("parentesco"));
        tableBene.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("email"));
        BeneficiarioDAO dao = new BeneficiarioDAO();
        int size = dao.selectAll(idCand).size();
        data.addAll(dao.selectAll(getIdCand()));
        tableBene.setItems(data);
    }

}
