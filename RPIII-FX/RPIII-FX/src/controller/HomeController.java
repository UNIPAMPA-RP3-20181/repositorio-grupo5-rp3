package controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.Period;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Candidato;
import model.Corretor;
import model.dao.DAOMestra;

/**
 *
 * @author roliv
 */
public class HomeController implements Initializable {

//    private static final String driver = "com.mysql.jdbc.Driver";
//    private static final String url = "jdbc:mysql://localhost:3306/corretora";
//    private static final String user = "root";
    //private static final String pass = "";
    //private String tipoUsuario;
    private String pagina;
    private Candidato candidato;
    private Corretor corretor;

    public void setCorretor(Corretor corretor) {
        this.corretor = corretor;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }

    @FXML
    private Button buttonCadastrar;

    @FXML
    private TextField username_box;

    @FXML
    private PasswordField password_box;

    @FXML
    private Label invalid_label;

    @FXML
    private Button buttonID;

    @FXML
    private Button buttonMorte;

    @FXML
    void abrirCadastro(ActionEvent event) throws IOException {

        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/CadastroCandidato.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();

    }

    @FXML
    void handleButtonAction(ActionEvent event) throws IOException, Exception {
        DAOMestra dao = new DAOMestra();
        int tipoUsuario = dao.validarValores(username_box.getText(), password_box.getText());
        if (tipoUsuario == 1) {
            setCandidato(dao.loginCandidato(username_box.getText(), password_box.getText()));
        }else if(tipoUsuario == 2){
            setCorretor(dao.loginCorretor(username_box.getText(), password_box.getText()));
        }

        switch (tipoUsuario) {
            case 1: {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/menuCandidato.fxml"));
                Parent menu_parent = loader.load();
                MenuCandidatoController controller = loader.<MenuCandidatoController>getController();
                Candidato can = this.candidato;
                ControllerAbs.setIdCand(can.getId());
                controller.setNomeLabel(can.getNome());
                controller.setDataNascimento(can.getDataNascimento());
                controller.setGenLabel(can.getGenero());
                controller.setEndLabel(can.getEnd().getRua());
                controller.setNumeroLabel(can.getEnd().getNumero());
                controller.setCepLabel(can.getEnd().getCep());
                controller.setTelLabel(can.getTelefone());
                controller.setCelLabel(can.getCelular());
                controller.setOcupLabel(can.getOcupacao());
                controller.setEmailLabel(can.getUsuario().getEmailCadastro());
                controller.setBairro(can.getEnd().getBairro());
                controller.setCidade(can.getEnd().getCidade());
                controller.setEstado(can.getEnd().getEstado());
                controller.setEstadoCivil(can.getEstadoCivil());
                String[] data = can.getDataNascimento().split("/");
                LocalDate birthDate = LocalDate.of(Integer.parseInt(data[2]), Integer.parseInt(data[1]), Integer.parseInt(data[0]));
                controller.setIdadeLabel(String.valueOf(calculateAge(birthDate)));
                Scene sceneHome = ((Node) event.getSource()).getScene();
                controller.setSceneHome(sceneHome);
                Scene menu_scene = new Scene(menu_parent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(menu_scene);
                app_stage.show();

                break;
            }
            case 2: {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/MenuCorretor.fxml"));
                Parent menu_parent = loader.load();
                MenuCorretorController controller = loader.<MenuCorretorController>getController();
                Corretor cor = this.corretor;
                controller.setNomeCorretor(cor.getNome());
                controller.setEmailLabel(cor.getUsuario().getEmailCadastro());
                Scene menu_scene = new Scene(menu_parent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(menu_scene);
                app_stage.show();

                break;
            }
            case 3: {
                this.pagina = "/view/MenuCorretor.fxml";
                Scene menu_scene = getPage(pagina);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(menu_scene);
                app_stage.show();
                break;
            }
            default:
                username_box.clear();
                password_box.clear();
                invalid_label.setText("Nome de usuário e/ou senha inválidos!");
                break;
        }
    }

//    private boolean validarCredenciais() {
//        boolean logar = false;
//        System.out.println("SELECT * FROM Usuario WHERE nomeUsuario = " + "'" + username_box.getText()
//                + "'" + " AND senhaUsuario= " + "'" + password_box.getText() + "'");
//        Connection c = null;
//        java.sql.Statement stmt = null;
//        try {
//            c = DriverManager.getConnection(url, user, pass);
//            c.setAutoCommit(false);
//
//            System.out.println("Conectado ao banco de dados com sucesso!");
//            stmt = c.createStatement();
//
//            ResultSet rs = stmt.executeQuery("SELECT * FROM Usuario WHERE nomeUsuario = " + "'" + username_box.getText()
//                    + "'" + " AND senhaUsuario= " + "'" + password_box.getText() + "'");
//
//            while (rs.next()) {
//                if (rs.getString("nomeUsuario") != null && rs.getString("senhaUsuario") != null) {
//                    String nomeUsuario = rs.getString("nomeUsuario");
//                    System.out.println("nomeUsuario = " + nomeUsuario);
//                    String senhaUsuario = rs.getString("senhaUsuario");
//                    System.out.println("senhaUsuario = " + senhaUsuario);
//                    this.tipoUsuario = rs.getString("tipoUsuario");
//                    DAOMestra dao = new DAOMestra<>();
//                    setCandidato(dao.validarValores(nomeUsuario, senhaUsuario));
//                    logar = true;
//
//                }
//            }
//
//            rs.close();
//            stmt.close();
//            c.close();
//        } catch (Exception e) {
//            System.err.println(e.getClass().getName() + ": " + e.getMessage());
//            System.exit(0);
//        }
//        System.out.println("Operação realizada com sucesso!");
//        return logar;
//    }
    public Scene getPage(String string) throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource(string));
        Scene menu_scene = new Scene(menu_parent);
        return menu_scene;
    }

    @FXML
    void relatarMorte(ActionEvent event) throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/RelatarMorte.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
    }

    public static int calculateAge(LocalDate birthDate) {
        LocalDate currentDate = LocalDate.now();
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
