/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import model.ItemProblemaSaude;
import model.ProblemaSaude;
import model.Solicitacao;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class SolicitacaoController extends ControllerAbs implements Initializable {

    private double totalFinal;
    private List<ProblemaSaude> problemas = new ArrayList();
    private double total;
    final ObservableList<ProblemaSaude> data = FXCollections.observableArrayList();
    private int idade;
    private double valorBase;
    private int gen;
    private double valorAcrecimo;

    public void setGen(int gen) {
        this.gen = gen;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public double getTotalFinal() {
        return totalFinal;
    }

    public void setTotalFinal(double totalFinal) {
        this.totalFinal = totalFinal;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @FXML
    private Button buttonVoltarPendente;

    @FXML
    private Button buttonCalc;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonRemove;

    @FXML
    private TableView<ProblemaSaude> tableView;

    @FXML
    private TextArea textArea;

    @FXML
    private TextField textField;

    @FXML
    private Slider preCancer;

    @FXML
    private Slider preDiabetes;

    @FXML
    private Slider preDemencia;

    @FXML
    private Slider preCoracao;

    @FXML
    private Slider preDegeneracao;

    @FXML
    private Slider preCerebral;

    @FXML
    private Slider prePulmonar;

    @FXML
    private CheckBox checkAlc;

    @FXML
    private CheckBox checkFum;

    @FXML
    private Label numLabel1;

    @FXML
    private Label numLabel2;

    @FXML
    private Label numLabel3;

    @FXML
    private Label numLabel4;

    @FXML
    private Label numLabel5;

    @FXML
    private Label numLabel6;

    @FXML
    private Label numLabel7;

    @FXML
    void adicionarDoenca() {
        if (Validacao.validarNome(textField.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite um nome Válido!");
            textField.setText("");

        } else if (Validacao.validarDescricao(textArea.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite a Descrição!");

        } else {
            ProblemaSaude problema = new ProblemaSaude(textField.getText(), textArea.getText());
            data.add(problema);
            textField.setText("");
            textArea.setText("");
        }
    }

    @FXML
    void calcularSolicitacao(ActionEvent event) throws IOException {
        if (idade <= 25) {
            if (gen == 0) {
                valorBase = 617.00;
                valorAcrecimo = valorBase * 0.1;
            } else {
                valorBase = 535.50;
                valorAcrecimo = valorBase * 0.1;
            }
        } else if (idade >= 26 && idade <= 35) {
            if (gen == 0) {
                valorBase = 585.80;
                valorAcrecimo = valorBase * 0.1;
            } else {
                valorBase = 516.20;
                valorAcrecimo = valorBase * 0.1;
            }
        } else if (idade >= 36 && idade <= 45) {
            if (gen == 0) {
                valorBase = 512.95;
                valorAcrecimo = valorBase * 0.1;
            } else {
                valorBase = 478.95;
                valorAcrecimo = valorBase * 0.1;
            }
        } else if (idade >= 46 && idade <= 50) {
            if (gen == 0) {
                valorBase = 557.00;
                valorAcrecimo = valorBase * 0.1;
            } else {
                valorBase = 482.17;
                valorAcrecimo = valorBase * 0.1;
            }
        } else if (idade >= 51 && idade <= 55) {
            if (gen == 0) {
                valorBase = 589.94;
                valorAcrecimo = valorBase * 0.1;
            } else {
                valorBase = 488.90;
                valorAcrecimo = valorBase * 0.1;
            }
        } else if (idade >= 56 && idade <= 60) {
            if (gen == 0) {
                valorBase = 624.90;
                valorAcrecimo = valorBase * 0.1;
            } else {
                valorBase = 524.30;
                valorAcrecimo = valorBase * 0.1;
            }
        } else if (idade >= 61 && idade <= 65) {
            if (gen == 0) {
                valorBase = 656.66;
                valorAcrecimo = valorBase * 0.1;
            } else {
                valorBase = 544.41;
                valorAcrecimo = valorBase * 0.1;
            }
        } else if (idade >= 66 && idade <= 75) {
            if (gen == 0) {
                valorBase = 687.00f;
                valorAcrecimo = valorBase * 0.1;
            } else {
                valorBase = 581.50f;
                valorAcrecimo = valorBase * 0.1;
            }
        } else if (idade >= 76 && idade <= 85) {
            if (gen == 0) {
                valorBase = 714.32;
                valorAcrecimo = valorBase * 0.1;
            } else {
                valorBase = 644.12;
                valorAcrecimo = valorBase * 0.1;
            }
        } else if (idade >= 86 && idade <= 90) {
            if (gen == 0) {
                valorBase = 851.60;
                valorAcrecimo = valorBase * 0.1;
            } else {
                valorBase = 792.70;
                valorAcrecimo = valorBase * 0.1;
            }
        }
        double a = (31.64 * preCancer.getValue() * 0.1) + (31.64 * preCerebral.getValue() * 0.1)
                + (31.64 * preCoracao.getValue() * 0.1) + (31.64 * preDegeneracao.getValue() * 0.1)
                + (31.64 * preDemencia.getValue() * 0.1) + (31.64 * preDiabetes.getValue() * 0.1)
                + (31.64 * prePulmonar.getValue() * 0.1) + (31.64 * tableView.getItems().size());

        if (a >= 126.56) {
            JOptionPane.showMessageDialog(null, "Não é possível fazer a solicitação.\nA quantidade de predisposições e doenças não é permitida.");
        } else {

            if (checkAlc.isSelected()) {
                a = a + 296.36;
            }
            if (checkFum.isSelected()) {
                a = a + 203.09;
            }

            if (a >= 562.73) {
                JOptionPane.showMessageDialog(null, "Não é possível fazer a solicitação.\nPara fumantes e alcoolista, a quantidade de predisposições e doenças não é permitida.");
            } else {

                valorBase = valorBase + a;
                double valorAcres = 0;
                double valorPremio = valorBase * 12;
                if (valorPremio > 150000) {
                    valorPremio -= 150000;
                    valorAcres = (Math.ceil(valorPremio / 25000)) * valorAcrecimo;
                }
                valorBase = valorBase + valorAcres;
                valorBase = Double.parseDouble(new DecimalFormat("0.00").format(valorBase));

                while (!tableView.getItems().isEmpty()) {
                    problemas.add(new ProblemaSaude(tableView.getItems().get(0).getNomeProbSaude(),
                            tableView.getItems().get(0).getDescricao()));
                    tableView.getItems().remove(0);
                }

                ItemProblemaSaude itens = new ItemProblemaSaude();
                itens.setProblemas(problemas);

                Solicitacao solicitacao = new Solicitacao();
                solicitacao.setItens(itens);
                solicitacao.setPreCancer((int) preCancer.getValue());
                solicitacao.setPreCerebral((int) preCerebral.getValue());
                solicitacao.setPreCoracao((int) preCoracao.getValue());
                solicitacao.setPreDemencia((int) preDemencia.getValue());
                solicitacao.setPreDiabetes((int) preDiabetes.getValue());
                solicitacao.setPreDegeneracao((int) preDegeneracao.getValue());
                solicitacao.setPrePulmonar((int) prePulmonar.getValue());
                solicitacao.setAlcoolatra(checkAlc.isSelected());
                solicitacao.setFumante(checkFum.isSelected());
                solicitacao.setValor(valorBase);
                solicitacao.setIdCandidato(getIdCand());
                LocalDate currentDate = LocalDate.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                String dataSol = currentDate.format(formatter);
                solicitacao.setDataSolicitacao(dataSol);
                solicitacao.setStatusAvaliador(3);
                solicitacao.setStatusCorretor(3);

                super.empilharAnte(((Node) event.getSource()).getScene());

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CalculoSeguro.fxml"));
                Parent menu_parent = loader.load();

                CalculoSeguroController controller = loader.<CalculoSeguroController>getController();
                controller.setSolicitacao(solicitacao);
                controller.setTotal(valorBase);

                Scene menu_scene = new Scene(menu_parent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(menu_scene);
                app_stage.show();
            }
        }
    }

    @FXML
    void voltarPendente(ActionEvent event) throws IOException {
        empilharProx(((Node) event.getSource()).getScene());
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(super.desempilharAnte());
        app_stage.show();

    }

    @FXML
    void removerDoenca(ActionEvent event
    ) {
        tableView.getItems().removeAll(tableView.getSelectionModel().getSelectedItems());
    }

    @Override
    public void initialize(URL url, ResourceBundle rb
    ) {

        setBindings(numLabel1, preCancer);
        setBindings(numLabel2, preDiabetes);
        setBindings(numLabel3, preDemencia);
        setBindings(numLabel4, preCoracao);
        setBindings(numLabel5, preDegeneracao);
        setBindings(numLabel6, preCerebral);
        setBindings(numLabel7, prePulmonar);

        tableView.setPlaceholder(new Label("Nenhum problema de saúde foi adicionado"));

        tableView.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("nomeProbSaude"));
        tableView.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("descricao"));
        tableView.setItems(data);

    }

    private void setBindings(Label label, Slider slider) {
        label.textProperty().bind(Bindings.format(
                "%.0f",
                slider.valueProperty()
        ));
    }

}
