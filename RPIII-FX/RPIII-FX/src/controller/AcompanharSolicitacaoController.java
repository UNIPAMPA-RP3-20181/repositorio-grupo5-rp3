/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Solicitacao;
import model.dao.SolicitacaoDAO;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class AcompanharSolicitacaoController extends ControllerAbs implements Initializable {
    
    final ObservableList<Solicitacao> data = FXCollections.observableArrayList();

    @FXML
    private Button buttonVoltarSolPendente;

    @FXML
    private TableView<Solicitacao> tablePendente;


    @FXML
    void voltarSolPendente(ActionEvent event) throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/SolicitacoesPendentes.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tablePendente.setPlaceholder(new Label("Nenhuma solicitação pendente."));
        tablePendente.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("dataSolicitacao"));
        tablePendente.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("valor"));
        tablePendente.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("statusCorretor"));
        tablePendente.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("statusAvaliador"));
        SolicitacaoDAO dao = new SolicitacaoDAO();
        data.add(dao.select(getIdCand()));
        tablePendente.setItems(data);
    }    
    
}
