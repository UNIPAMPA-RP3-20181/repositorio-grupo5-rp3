/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import model.RelatoMorte;
import model.dao.RelatoMorteDAO;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class RelatarMorteController implements Initializable {

    private File file;

    @FXML
    private ImageView imgUser;

    @FXML
    private TextField nomeSegInput;

    @FXML
    private TextField numApoliceInput;

    @FXML
    private TextField causaMorteInput;

    @FXML
    private Button buttonAnexar;

    @FXML
    private TextField nomeInput;

    @FXML
    private TextField parentInput;

    @FXML
    private TextField telInput;

    @FXML
    private TextField emailInput;

    @FXML
    private Button buttonSair;

    @FXML
    private Button buttonRelatar;

    @FXML
    void anexarPDF(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("pdf files (*.pdf)", "*.pdf");
        fileChooser.getExtensionFilters().add(extFilter);
        File selectedFile = fileChooser.showOpenDialog(null);

        if (selectedFile != null) {

            JOptionPane.showMessageDialog(null, "Arquivo selecionado");
            this.file = selectedFile;

        } else {

            JOptionPane.showMessageDialog(null, "Arquivo não selecionado");

        }
    }

    @FXML
    void relatarMorte(ActionEvent event) throws IOException {
        if (Validacao.validarDescricao(nomeInput.getText()) != true
                || Validacao.validarDescricao(nomeSegInput.getText()) != true
                || Validacao.validarDescricao(causaMorteInput.getText()) != true
                || Validacao.validarDescricao(parentInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        } else if (Validacao.validarNome(nomeInput.getText()) != true
                || Validacao.validarNome(nomeSegInput.getText()) != true
                || Validacao.validarNome(causaMorteInput.getText()) != true
                || Validacao.validarNome(parentInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite apenas valores válidos!");
        } else if (Validacao.validarEmail(emailInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite um e-mail válido!");
            emailInput.setText("");
        } else if (file == null) {
            JOptionPane.showMessageDialog(null, "Você precisa selecionar o Certificado de Obito");
        } else {

            RelatoMorte relatoMorte = new RelatoMorte(nomeSegInput.getText(),
                    Integer.parseInt(numApoliceInput.getText()), causaMorteInput.getText(), nomeInput.getText(),
                    parentInput.getText(), telInput.getText(), emailInput.getText(), this.file);
            RelatoMorteDAO gravarRelato = new RelatoMorteDAO();
            gravarRelato.insert(relatoMorte);
            JOptionPane.showMessageDialog(null, "Relato feito com sucesso!!");
            Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/FXMLHomePage.fxml"));
            Scene menu_scene = new Scene(menu_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(menu_scene);
            app_stage.show();
        }
    }

    @FXML
    void sairRelato(ActionEvent event) throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/FXMLHomePage.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MaskField.foneField(telInput);
        MaskField.numericField(numApoliceInput);
    }

}
