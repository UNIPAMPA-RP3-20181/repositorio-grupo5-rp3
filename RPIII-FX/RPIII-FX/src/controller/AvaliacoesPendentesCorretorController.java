/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static controller.ControllerAbs.idCand;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Solicitacao;
import model.dao.SolicitacaoDAO;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class AvaliacoesPendentesCorretorController implements Initializable {
    final ObservableList<Solicitacao> data = FXCollections.observableArrayList();

    @FXML
    private Button buttonVoltar;

    @FXML
    private Button buttonAvaliar;

    @FXML
    private TableView<Solicitacao> tableSolicitação;

    @FXML
    void avaliarSolicitacaoCorretor(ActionEvent event) {

    }

    @FXML
    void voltar(ActionEvent event) throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/MenuCorretor.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();

    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tableSolicitação.setPlaceholder(new Label("Nenhuma solicitação pendente."));
        tableSolicitação.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("valor"));
        tableSolicitação.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("dataSolicitacao"));
        SolicitacaoDAO dao = new SolicitacaoDAO();
        int size = dao.selectAll(idCand).size();
        data.addAll(dao.selectAll(2));
        tableSolicitação.setItems(data);
    }    
    
}
