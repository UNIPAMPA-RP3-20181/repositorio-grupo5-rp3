/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Usuario;
import model.dao.UsuarioDAO;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class MenuCandidatoController extends ControllerAbs implements Initializable {

    private Scene sceneHome;
    private String bairro;
    private String cidade;
    private String estado;
    private String estadoCivil;
    private String dataNascimento;

    public Scene getSceneHome() {
        return sceneHome;
    }

    public void setSceneHome(Scene sceneHome) {
        this.sceneHome = sceneHome;
    }    

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @FXML
    private ImageView imagePerfil;

    @FXML
    private Label nomeLabel;

    @FXML
    private Label idadeLabel;

    @FXML
    private Label genLabel;

    @FXML
    private Button buttonEditar;

    @FXML
    private Button buttonUsuario;

    @FXML
    private Label endLabel;

    @FXML
    private Label numeroLabel;

    @FXML
    private Label cepLabel;

    @FXML
    private Label telLabel;

    @FXML
    private Label celLabel;

    @FXML
    private Label ocupLabel;

    @FXML
    private Label emailLabel;

    @FXML
    private Button buttonBen;

    @FXML
    private Button buttonSol;

    @FXML
    private Button buttonExit;

    public void setImagePerfil(ImageView imagePerfil) {
        this.imagePerfil = imagePerfil;
    }

    public void setNomeLabel(String nome) {
        this.nomeLabel.setText(nome);
    }

    public void setIdadeLabel(String idade) {
        this.idadeLabel.setText(idade);
    }

    public void setGenLabel(String genero) {
        this.genLabel.setText(genero);
    }

    public void setEndLabel(String endereco) {
        this.endLabel.setText(endereco);
    }

    public void setNumeroLabel(String numero) {
        this.numeroLabel.setText(numero);
    }

    public void setCepLabel(String cep) {
        this.cepLabel.setText(cep);
    }

    public void setTelLabel(String tel) {
        this.telLabel.setText(tel);
    }

    public void setCelLabel(String cel) {
        this.celLabel.setText(cel);
    }

    public void setOcupLabel(String ocupacao) {
        this.ocupLabel.setText(ocupacao);
    }

    public void setEmailLabel(String email) {
        this.emailLabel.setText(email);
    }

    @FXML
    void editarPerfil(ActionEvent event) throws IOException {

        super.empilharAnte(((Node) event.getSource()).getScene());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/EditarCandidato.fxml"));
        Parent menu_parent = loader.load();
        EditarCandidatoController controller = loader.<EditarCandidatoController>getController();
        controller.setNomeInput(nomeLabel.getText());
        controller.setBairroInput(this.bairro);
        controller.setCelInput(celLabel.getText());
        controller.setCepInput(cepLabel.getText());
        controller.setCidInput(this.cidade);
        controller.setCivilCombo(estadoCivil);
        controller.setDataImput(dataNascimento);
        controller.setEstInput(estado);
        controller.setImgUser(imagePerfil);
        controller.setNumInput(numeroLabel.getText());
        controller.setOcupInput(ocupLabel.getText());
        controller.setRuaInput(endLabel.getText());
        controller.setTelInput(telLabel.getText());
        controller.setGenero(genLabel.getText());
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
    }
    
    @FXML
    void editarUsuario(ActionEvent event) throws IOException {
        UsuarioDAO dao = new UsuarioDAO();
        Usuario user = new Usuario();
        user.setEmailCadastro(dao.select(idCand).getEmailCadastro());
        user.setNomeLogin(dao.select(idCand).getNomeLogin());
        user.setSenhaLogin(dao.select(idCand).getSenhaLogin());
        super.empilharAnte(((Node) event.getSource()).getScene());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/EditarUsuario.fxml"));
        Parent menu_parent = loader.load();
        EditarUsuarioController controller = loader.<EditarUsuarioController>getController();
        controller.setEmailInput(user.getEmailCadastro());
        controller.setUsuarioInput(user.getNomeLogin());
        controller.setSenhaInput(user.getSenhaLogin());
        controller.setConfirmInput(user.getSenhaLogin());
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();

    }

    @FXML
    void gerenciarBeneficiarios(ActionEvent event) throws IOException {
        
        super.empilharAnte(((Node) event.getSource()).getScene());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/GerenciarBeneficiarios.fxml"));
        Parent menu_parent = loader.load();
        
        if (proxVazia()) {
            Scene menu_scene = new Scene(menu_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(menu_scene);
            app_stage.show();
        } else {
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(super.desempilharProx());
            app_stage.show();
        }
    }

    @FXML
    void sairMenu(ActionEvent event) throws IOException {
        esvaziarPilhas();
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/FXMLHomePage.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
    }

    @FXML
    void solicitarSeguro(ActionEvent event) throws IOException {
        super.empilharAnte(((Node) event.getSource()).getScene());
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/SolicitacoesPendentes.fxml"));
        Parent menu_parent = loader.load();
        SolicitacoesPendentesController controller = loader.<SolicitacoesPendentesController>getController();
        controller.setIdade(Integer.parseInt(idadeLabel.getText()));
        controller.setGenero(genLabel.getText());

        if (proxVazia()) {
            Scene menu_scene = new Scene(menu_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(menu_scene);
            app_stage.show();
        } else {
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(super.desempilharProx());
            app_stage.show();
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }
}
