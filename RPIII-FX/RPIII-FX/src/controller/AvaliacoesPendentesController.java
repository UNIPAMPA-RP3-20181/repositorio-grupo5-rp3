/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import model.Solicitacao;
/**
 *
 * @author roliv
 */
public class AvaliacoesPendentesController {
    
    final ObservableList<Solicitacao> data = FXCollections.observableArrayList();
    
    @FXML
    private Button buttonVoltarPendente;

    @FXML
    private Button buttonAvaliar;

    @FXML
    private TableView<?> tabelaSolicitacoes;

    @FXML
    void avaliarSolicitacao(ActionEvent event) {

    }

    @FXML
    void voltarPendente(ActionEvent event) throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/menuAvaliador.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
        
    }
    
    public void initialize(URL url, ResourceBundle rb) {
        
       // tabelaSolicitacoes.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("nome"));
     //   tabelaSolicitacoes.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("descricao"));
     //   tabelaSolicitacoes.setItems(data);
       // Solicitacao solicitacao = new Doenca(textField.getText(),textArea.getText());
        //data.add(doenca);
        
        
    }

}