/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 *
 * @author roliv
 */
public class MenuAvaliadorController {
    
    @FXML
    private Label nomeAvaliador;

    @FXML
    private Label idade;

    @FXML
    private Label sexo;

    @FXML
    private Button buttonEdiPerfil;

    @FXML
    private Label telefone;

    @FXML
    private Label email;

    @FXML
    private Button buttonPendentes;

    @FXML
    private Button buttonSinistro;

    @FXML
    private Button buttonExit;

    @FXML
    void avaliarSinistro(ActionEvent event) throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/SinistrosPendentes.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
    }

    @FXML
    void avaliarPendentes(ActionEvent event) throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/AvaliacoesPendentes.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
        
    }
    
    @FXML
    void editarPerfil(ActionEvent event) throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/EditarAvalidador.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
    }
    
    @FXML
    void sairMenu(ActionEvent event)throws IOException {
        Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/FXMLHomePage.fxml"));
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
    }
}
