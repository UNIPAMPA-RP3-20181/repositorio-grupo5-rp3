/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import model.Beneficiario;
import model.Candidato;
import model.Endereco;
import model.Usuario;
import model.dao.DAOMestra;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class CadastroFinalController extends ControllerAbs implements Initializable {

    private Candidato candidato;
    private List<Beneficiario> listaBeneficiario;
    private Image imagePerfil;
    private String nome;
    private String dataNascimento;
    private String cpfInput;
    private String genero;
    private String estadoCivil;
    private String ocupacao;
    private String ruaInput;
    private String bairroInput;
    private String cidadeInput;
    private String telInput;
    private String emailInput;
    private String numInput;
    private String cepInput;
    private String estInput;
    private String celInput;
    private String complementoInput;

    public void setImagePerfil(Image imagePerfil) {
        this.imagePerfil = imagePerfil;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public void setCpfInput(String cpfInput) {
        this.cpfInput = cpfInput;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public void setOcupacao(String ocupacao) {
        this.ocupacao = ocupacao;
    }

    public void setRuaInput(String ruaInput) {
        this.ruaInput = ruaInput;
    }

    public void setBairroInput(String bairroInput) {
        this.bairroInput = bairroInput;
    }

    public void setCidadeInput(String cidadeInput) {
        this.cidadeInput = cidadeInput;
    }

    public void setTelInput(String telInput) {
        this.telInput = telInput;
    }

    public void setEmailInput(String emailInput) {
        this.emailInput = emailInput;
    }

    public void setNumInput(String numInput) {
        this.numInput = numInput;
    }

    public void setCepInput(String cepInput) {
        this.cepInput = cepInput;
    }

    public void setEstInput(String estInput) {
        this.estInput = estInput;
    }

    public void setCelInput(String celInput) {
        this.celInput = celInput;
    }

    public void setComplementoInput(String complementoInput) {
        this.complementoInput = complementoInput;
    }

    @FXML
    private TextField usernameInput;

    @FXML
    private PasswordField passInput;

    @FXML
    private PasswordField passConfInput;

    @FXML
    private Button buttonVoltar;

    @FXML
    private Button buttonCadastrar;

    @FXML
    void relizarCadastro(ActionEvent event) throws IOException {
        if (Validacao.validarDescricao(usernameInput.getText()) != true
                || Validacao.validarDescricao(passInput.getText()) != true
                || Validacao.validarDescricao(passConfInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        } else if (Validacao.validarChar(usernameInput.getText()) != true
                || Validacao.validarChar(passInput.getText()) != true
                || Validacao.validarChar(passConfInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite apenas valores válidos!");
        } else if (Validacao.validarSenha(passInput.getText(), passConfInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "A senha e confirmação de senha são diferentes!");
        } else if (Validacao.validarTamanhoSenha(passInput.getText())) {
            JOptionPane.showMessageDialog(null, "A senha deve ter no mínimo 6 caracteres!");
        } else {
            this.candidato = new Candidato(new Usuario(usernameInput.getText(), passInput.getText(), emailInput),
                    listaBeneficiario, ocupacao, imagePerfil, nome, cpfInput, telInput, celInput, genero, estadoCivil,
                    dataNascimento, new Endereco(ruaInput, numInput, complementoInput, cepInput, bairroInput, cidadeInput, estInput));
            DAOMestra mestra = new DAOMestra();
            if (mestra.RegistrarCandidato(candidato)) {
                JOptionPane.showMessageDialog(null, "Cadastro feito com sucesso!");
                esvaziarPilhas();

                Parent menu_parent = FXMLLoader.load(getClass().getResource("/view/FXMLHomePage.fxml"));
                Scene menu_scene = new Scene(menu_parent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(menu_scene);
                app_stage.show();
            }

        }
    }

    @FXML
    void voltarCadastro(ActionEvent event) throws IOException {
        empilharProx(((Node) event.getSource()).getScene());
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(super.desempilharAnte());
        app_stage.show();

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
