/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import model.Beneficiario;
import model.dao.DAOMestra;

/**
 * FXML Controller class
 *
 * @author roliv
 */
public class AdicionarBeneficiarioController extends ControllerAbs implements Initializable {
    
    private ToggleGroup group = new ToggleGroup();
    private ObservableList<String> estadoCivil = FXCollections.observableArrayList("Conjugue", "Filho(a)",
            "Pai/Mãe", "Irmão(ã)", "Outro(a)");

    @FXML
    private TextField nomeInput;

    @FXML
    private ComboBox ParentescoCombo;

    @FXML
    private TextField contatoInput;

    @FXML
    private TextField emailInput;

    @FXML
    private Button buttonVoltar;

    @FXML
    private Button buttonConfirm;

    @FXML
    void confirmarAdicionar(ActionEvent event) throws IOException {
        if (Validacao.validarDescricao(nomeInput.getText()) != true
                || ParentescoCombo.getValue().equals("Selecione")
                || Validacao.validarDescricao(contatoInput.getText()) != true
                || Validacao.validarDescricao(emailInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
        } else if (Validacao.validarNome(nomeInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite apenas valores válidos!");
        } else if (Validacao.validarEmail(emailInput.getText()) != true) {
            JOptionPane.showMessageDialog(null, "Digite o email do contaro!!");
        } else {
            Beneficiario beneficiario = new Beneficiario();
            beneficiario.setContato(contatoInput.getText());
            beneficiario.setEmail(emailInput.getText());
            beneficiario.setNome(nomeInput.getText());
            beneficiario.setParentesco(String.valueOf(this.ParentescoCombo.getValue()));
            beneficiario.setIdCandidato(idCand);
            DAOMestra dao = new DAOMestra();
            if (dao.RegistrarBeneficiario(beneficiario)) {
                JOptionPane.showMessageDialog(null, "Beneficiário adicionado com sucesso!");
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/GerenciarBeneficiarios.fxml"));
                Parent menu_parent = loader.load();
                Scene menu_scene = new Scene(menu_parent);
                Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                app_stage.hide();
                app_stage.setScene(menu_scene);
                app_stage.show();
            } else {
                JOptionPane.showMessageDialog(null, "Não foi possível adicionar Beneficiário!");
            }
        }
    }

    @FXML
    void voltarCandidato(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/GerenciarBeneficiarios.fxml"));
        Parent menu_parent = loader.load();
        Scene menu_scene = new Scene(menu_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(menu_scene);
        app_stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ParentescoCombo.setValue("Selecione");
        ParentescoCombo.setItems(estadoCivil);
        MaskField.foneField(contatoInput);
    }

}
